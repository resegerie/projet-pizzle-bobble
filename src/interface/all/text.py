from interface import *
from maths import *

class uiText(uiElement):
    __text = ""
    __font = None
    colorBackground = None
    antiAliasing = False

    def get_font(self): return self.__font
    def set_font(self, font):
        if(font is not None and font == self.__font):
            return
        self.__font = fonts.font_main if font is None else font
        self.render_text()
    font = property(get_font, set_font) 

    def color_changed(self):
        self.render_text()

    __text_image = None

    def render_text(self):
        self.set_text(self.text) # not a bug, it force the text to write itself again

    def get_text(self): return self.__text
    def set_text(self, txt):
        """ also force the text to render itself on a image"""
        self.__text = txt
        if(self.font is not None):
            self.__text_image = self.font.render(self.__text, self.antiAliasing, self.color, self.colorBackground)
            text_rect_obj = self.__text_image.get_rect()
            self.hitbox =  hvec2(0, text_rect_obj.w/text_rect_obj.h*self.hitbox.y, self.hitbox.y, 0)
    text = property(get_text, set_text) 

    def get_size(self): return self.hitbox.y
    def set_size(self, size):
        self.hitbox.y = size
        self.render_text()
    size = property(get_size, set_size) 

    def with_size(self, size = fonts.normal):
        self.size = size
        return self

    def __init__(self, txt, name = "unknow", size = fonts.normal, font = None):
        #self.colorBackground = color.DARK_GREEN # for debugging purpose
        super().__init__(name)
        self.text = txt
        self.with_size(size)
        self.font = font

    def with_colorBackground(self, colorBackground):
        if(self.colorBackground != colorBackground):
            self.colorBackground = colorBackground
            self.render_text()
        return self

    def draw(self):
        cam.draw_image(self.__text_image, self.pos, self.hitbox, self.center)
        #cam.draw_font(self.pos, self.text, colorForeground=self.color, colorBackground=self.colorBackground, font=self.font, antiAliasing=self.antiAliasing, size=self.size, center=self.center)
