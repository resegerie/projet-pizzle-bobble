from interface import *
from maths import *

class uiSprite(uiElement):
    img = None
    flipX, flipY= (False, False)

    def __init__(self, img, name = "unknow"):
        super().__init__(name)
        self.img = get_image(img) if type(img) == str else img
        self.with_flipX(False).with_flipY(False)

    def with_flipX(self, flipX):
        self.flipX = flipX
        return self

    def with_flipY(self, flipY):
        self.flipY = flipY
        return self

    def draw(self):
        cam.draw_sprite(self.img, self.pos, self.hitbox, self.center, flipX=self.flipX, flipY=self.flipY)

class uiImage(uiSprite):
    rectangleOriginPx, rectangleSizePx= (None, None)

    def __init__(self, img, name = "unknow", rectangleOriginPx = vec2(0) , rectangleSizePx = None):
        super().__init__(img, name)
        self.rectangleOriginPx=rectangleOriginPx
        self.rectangleSizePx=rectangleSizePx

    def draw(self):
        cam.draw_image(self.img, self.pos, self.hitbox, self.center, vec2rectangleOriginPx=self.rectangleOriginPx, vec2rectangleSizePx=self.rectangleSizePx, flipX=self.flipX, flipY=self.flipY)

    
class uiWallpaper(uiSprite):

    def __init__(self, img, name = "unknow"):
        super().__init__(img, name)
        self.pos = hvec2(0,0,0,0)
        self.center = vec2(0,0)

    def draw(self):
        pos_pixel = self.pos.to_vec2().to_tuple_int() 

        h_pixel = self.hitbox.to_vec2()
        #sub pixel trick to avoid 1 pixel offset from 2 tiles because the size is an integer
        hx, hy = ceil(h_pixel.x), ceil(h_pixel.y) 
        
        px, py = pos_pixel
        
        sprite = pygame.transform.scale(self.img, (hx, hy))
        sprite = pygame.transform.flip(sprite, self.flipX, self.flipY)
        
        while(py < game.height):
            px = pos_pixel[0]
            while(px < game.width):
                game.screen.blit(sprite, (px, py))
                px += hx
            py += hy

        #cam.draw_sprite(self.img, self.pos+self.hitbox, self.hitbox, self.center, flipX=self.flipX, flipY=self.flipY)

class uiWallPaperBorder(uiImage):
    
    def __init__(self, img, name = "unknow", rectangleSizePx = vec2(32), color = (0, 0, 0)):
        super().__init__(img, name)
        self.rectangleSizePx=rectangleSizePx
        self.color = color

    def draw(self):
        game.screen.fill(self.color)
        w = int(game.width/self.rectangleSizePx.x)
        h = int(game.height/self.rectangleSizePx.y)
        center = vec2(0)
        for i in range(w):
            for j in range(h):
                rectangleOriginPx=vec2(self.rectangleSizePx.x, self.rectangleSizePx.y)
                if(0==i):
                    rectangleOriginPx.x -= self.rectangleSizePx.x
                elif(w-1==i):
                    rectangleOriginPx.x += self.rectangleSizePx.x

                if(0==j):
                    rectangleOriginPx.y -= self.rectangleSizePx.y
                elif(h-1==j):
                    rectangleOriginPx.y += self.rectangleSizePx.y
                
                rectangleSizeAdjust = vec2(game.width/w, game.height/h)

                cam.draw_image(self.img, vec2(i*rectangleSizeAdjust.x, j*rectangleSizeAdjust.y), rectangleSizeAdjust, center, rectangleOriginPx, rectangleSizeAdjust)