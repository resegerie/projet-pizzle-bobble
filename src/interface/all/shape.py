from interface import *
from maths import *

class uiRect(uiElement):
    def draw(self):
        cam.draw_rect(self.pos, self.hitbox, self.color, self.center)

class uiEllipse(uiElement):
    def draw(self):
        cam.draw_ellipse(self.pos, self.hitbox, self.color, self.center)