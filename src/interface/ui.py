from interface import *
from maths import *
import interface.cameraman
from content_manager import *

class uiElement:
    name = None
    """string"""

    pos = None
    """vec2 for pixel coordinate, hvec2 for pourcent"""
    hitbox = None
    """vec2 for pixel coordinate, hvec2 for pourcent"""
    center = None
    """vec2 for pourcent (no support for hvec2)"""

    __color = None
    def get_color(self): return self.__color
    def set_color(self, color):
        if(self.__color != color):
            self.__color = color
            self.color_changed() # used by uiText
    color = property(get_color, set_color) 

    def color_changed(self):
        pass

    def copyPos(self, toCopy):
        self.pos = toCopy.pos.copy()
        return self
    def copyHitbox(self, toCopy):
        self.hitbox = toCopy.hitbox.copy()
        return self
    def copyCenter(self, toCopy):
        self.center = toCopy.center.copy()
        return self

    def get_pos_top_left(self):
        l = self.hitbox.to_vec2()
        return self.pos.to_vec2()-l*self.center
    def set_pos_top_left(self, pos):
        self.pos = pos+self.center*self.hitbox
    pos_top_left = property(get_pos_top_left, set_pos_top_left) 
    
    # builder pattern
    def with_pos(self, pos):
        self.pos = pos
        return self

    def with_hitbox(self, hitbox):
        self.hitbox = hitbox
        return self

    def with_center(self, center):
        self.center = center
        return self

    def with_color(self, color):
        self.color = color
        return self

    def get_pos_hitbox(self, hvec2HitboxCenter):
        return self.pos_top_left+self.hitbox*hvec2HitboxCenter
    
    def relative(self, ui, vec2_relatif = vec2(), hvec2_offset = hvec2()):
        self.copyPos(ui)
        self.pos = self.pos+ui.hitbox*hvec2(vec2_relatif.x, vec2_relatif.x, vec2_relatif.y, vec2_relatif.y)+hvec2_offset.to_hvec2()
        
        #self.pos = ui.pos_top_left+ui.hitbox*hvec2(vec2_relatif.x, vec2_relatif.x, vec2_relatif.y, vec2_relatif.y)+hvec2_offset
        return self

    def under(self, ui, hvec2_offset = hvec2()) : return self.relative(ui, vec2_relatif = vec2(0, 1) ,hvec2_offset = hvec2_offset)

    def __init__(self, name = "unknow"):
        self.pos = hvec2()
        self.hitbox = hvec2(0,0,0,0)
        self.center = vec2(0.5)
        self.color = color.WHITE
        self.name = name

    def load(self):
        pass

    def unload(self):
        pass

    def update(self):
        pass

    def draw(self):
        pass


class uiBag(uiElement):
    cam = None
    """camera"""

    __elements = None
    """list element"""

    def __init__(self, name = "unknow"):
        super().__init__(name)
        self.clear()
        self.cam = interface.cameraman.camera()

    def add(self, element):
        self.__elements.append(element)
        return element

    def remove(self, element_or_string):
        element_or_string = element_or_string if type(element_or_string) == str else element_or_string.name
        __elements = [i for i in __elements if i.name != element_or_string]

    def clear(self):
        self.__elements = []

    def load(self):
        pass

    def unload(self):
        for v in self.__elements:
            v.unload()

    def update(self):
        for v in self.__elements:
            v.update()

    def draw(self):
        for v in self.__elements:
            v.draw()