from maths import *
from content_manager import *
import pygame, game

camList = None
cam = None

class camera:
    pos    = None
    """convertible to vec2 (vec2 or hvec2)"""

    center = None
    """convertible to vec2 (vec2 or hvec2)"""

    zoom   = None
    """float"""

    def __init__(self):
        self.pos = vec2(0)
        self.center = hvec2(0.5, 0, 0.5, 0)
        self.zoom = 1
        self.update()

    def update(self):
        self.__pos_vec2 = self.pos.to_vec2()
        self.__center_vec2 = self.center.to_vec2()

    def push(self):
        global cam, camList
        camList.append(self.clone())

    def pop(self):
        global cam, camList
        c=camList.pop()
        self.pos = c.pos
        self.center = c.center
        self.zoom = c.zoom
        cam.update()

    def clone(self):
        c = camera()
        c.pos = self.pos.copy()
        c.center= self.center.copy()
        c.zoom = self.zoom
        return c

    def __str__(self):
        return f"pos:{self.pos}, zoom:{self.zoom}, center:{self.center}"

    def draw_sprite(self, sprite, pos, hitbox=vec2(1,1), center = vec2(0.5,0.5), flipX = False, flipY = False):
        """
        #sprite = pygame.transform.scale(sprite, (int(sprite.get_width()*self.zoom), int(sprite.get_height()*self.zoom)))
        sprite = pygame.transform.scale(sprite, (int(self.zoom), int(self.zoom)))
        game.screen.blit(sprite, (pos.x,pos.y))
        """

        
        p = (pos-center*hitbox).to_vec2()
        posTopLeftpyGame = (int((p.x-self.__center_vec2.x)*self.zoom-self.__pos_vec2.x+self.__center_vec2.x), int((p.y-self.__center_vec2.y)*self.zoom-self.__pos_vec2.y+self.__center_vec2.y))
        
        sprite = pygame.transform.scale(sprite, (int(self.zoom*hitbox.x), int(self.zoom*hitbox.y)))
        sprite = pygame.transform.flip(sprite, flipX, flipY)
        game.screen.blit(sprite, posTopLeftpyGame)
        
        #game.screen.blit(sprite, pos.to_tuple_int())
        #self.draw_image(sprite, pos, scale, center=center, flipX=flipX, flipY=flipY)

    def draw_image(self, image, pos, hitbox, center = vec2(0.5), vec2rectangleOriginPx = vec2(0,0), vec2rectangleSizePx = None, rotation=degree(0), flipX = False, flipY = False):
        l = hitbox.to_vec2()
        p = pos.to_vec2()-l*center
        posTopLeftpyGame = (int((p.x-self.__center_vec2.x)*self.zoom-self.__pos_vec2.x+self.__center_vec2.x), int((p.y-self.__center_vec2.y)*self.zoom-self.__pos_vec2.y+self.__center_vec2.y))
        r = image.get_size()
        w,h = r[0], r[1]
        vec2rectangleSizePx = vec2(w,h) if vec2rectangleSizePx is None else vec2rectangleSizePx

        ratio = (vec2(w,h)/vec2rectangleSizePx)
        l = (l*self.zoom*ratio).to_tuple_int()

        sprite = pygame.transform.scale(image, l)
        sprite = pygame.transform.flip(sprite, flipX, flipY)
        if(flipX):
            vec2rectangleOriginPx.x = w-vec2rectangleSizePx.x-vec2rectangleOriginPx.x
        if(flipY):
            vec2rectangleOriginPx.y = h-vec2rectangleSizePx.y-vec2rectangleOriginPx.y
        game.screen.blit(sprite, posTopLeftpyGame, (vec2rectangleOriginPx.x/w* l[0],vec2rectangleOriginPx.y/h*l[1],vec2rectangleSizePx.x/w* l[0],vec2rectangleSizePx.y/h*l[1]))

    def draw_square(self, pos, hitbox, color = color.WHITE, center = vec2(0.5)): self.draw_rect(pos, hitbox, color, center)
    def draw_rect(self, pos, hitbox, color = color.WHITE, center = vec2(0.5)):
        l = hitbox.to_vec2()
        p = pos.to_vec2()-l*center
        posTopLeftpyGame = (int((p.x-self.__center_vec2.x)*self.zoom-self.__pos_vec2.x+self.__center_vec2.x), int((p.y-self.__center_vec2.y)*self.zoom-self.__pos_vec2.y+self.__center_vec2.y), int(l.x*self.zoom), int(l.y*self.zoom))
        pygame.draw.rect(game.screen, color, posTopLeftpyGame)

    def draw_line(self, start, end, color = color.WHITE, width = 1):
        pygame.draw.line(game.screen, color, ((start.to_vec2()-self.__center_vec2)*self.zoom-self.__pos_vec2+self.__center_vec2).to_tuple(), ((end.to_vec2()-self.__center_vec2)*self.zoom-self.__pos_vec2+self.__center_vec2).to_tuple(), int(width*self.zoom))

    def draw_font(self, pos, text, colorForeground = color.WHITE, colorBackground = None, font = None, antiAliasing = False, size = fonts.normal, center = vec2(fonts.center, 0.5)):
        font = fonts.font_main if font is None else font
        text_surface_obj = font.render(text, antiAliasing, colorForeground, colorBackground)
        text_rect_obj = text_surface_obj.get_rect()
    
        center = center if type(center) == vec2 else vec2(center, 0.5)
        l = hvec2(0,text_rect_obj.w/text_rect_obj.h*size, size,0).to_vec2()
        p = pos.to_vec2()-l*center
        __center_vec2 = self.center.to_vec2()

        text_surface_obj = pygame.transform.scale(text_surface_obj, (l*self.zoom).to_tuple_int())  
        game.screen.blit(text_surface_obj, (((p-__center_vec2)*self.zoom)-self.pos.to_vec2()+__center_vec2).to_tuple_int())

    def draw_ellipse(self, pos, hitbox, color, center = vec2(0.5), stokeSize = 0):
        l = hitbox.to_vec2()
        p = pos.to_vec2()-l*center
        posTopLeftpyGame = (int((p.x-self.__center_vec2.x)*self.zoom-self.__pos_vec2.x+self.__center_vec2.x), int((p.y-self.__center_vec2.y)*self.zoom-self.__pos_vec2.y+self.__center_vec2.y), int(l.x*self.zoom), int(l.y*self.zoom))
        pygame.draw.ellipse(game.screen, color, posTopLeftpyGame, stokeSize)

camList = [camera()]
cam = camList[-1]
