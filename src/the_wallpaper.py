from content_manager import *
from interface import *

tiles = None

class wallpaper():
    uiWallpaper = None

    def __init__(self, uiWallpaper):
        self.uiWallpaper = uiWallpaper

    def update(self):
        pass

    def draw(self):
        self.uiWallpaper.draw()

def load_background():
    global tiles
    nbWallpaper = 6
    tiles = animation().from_rectangles(get_image("wallpaper/background"), nbWallpaper, 1, 1, 128, 128, 2, 2)