import pygame, game

def isKeyPressedEvent(key, type, events):
    for event in events:
        if event.type == type and event.key == key:
            return True
    return False

def isKeyPressed(key):
    """key is pressed"""
    return isKeyPressedEvent(key, pygame.KEYDOWN, game.oldEvents) 

def isKeyReleased(key):
    """key is not pressed"""
    return not isKeyPressed(key)

def isKeyPullUp(key):
    """key is now pressed"""
    #print("Old : ", game.oldEvents, "\nNew : ", game.events)
    return isKeyPressed(key) and isKeyPressedEvent(key, pygame.KEYUP, game.events)

def isKeyPullDown(key):
    """key is now released"""
    return isKeyReleased(key) and isKeyPressedEvent(key, pygame.KEYDOWN, game.events)

def getKeyName(key):
    """string representation of the key"""
    return pygame.key.name(key)#.replace("left", "<").replace("right", ">").replace("up", "^").replace("down", "v")