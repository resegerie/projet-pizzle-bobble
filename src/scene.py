from pygame.locals import *
from interface import *

class Scene:
    ui = None
    tick_loaded = None
    @property
    def tick(self): return game.tick-self.tick_loaded

    def __init__(self):
        self.ui = uiBag()
        pass #raise Exception("Scene est une classe abstraite, pas possible d'en crée une nouvelle")

    def _load(self):   # appeler lorsque le jeu passe sur cette scène 
        self.tick_loaded = game.tick 
        self.load()

    def _unload(self): # déchargement de la scène
        self.unload()
        self.ui.unload()

    def _update(self):
        self.ui.update()
        self.update()

    def _draw(self):
        self.draw()
        self.ui.draw()

    def load(self): 
        pass

    def unload(self):
        pass

    def update(self):
        pass

    def draw(self):
        pass

current = None # la scene actuel

def change(scene):
    global current
    print("scene changed:", type(current).__name__, "->", type(scene).__name__)
    if(current == scene):
        return
    if(current is not None):
        current._unload()
    current = scene
    if(current is not None):
        current._load()