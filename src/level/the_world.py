import random
from maths import *
from content_manager import *
from interface import *
from level.elements.the_ball import ball, ball_id_connector, STATE_STATIC, epsilon, STATE_FALLING
from level.elements.the_launcher import launcher
import scene, scenes.win
import time
from players import *

class world:
    elements   = None

    width, height = None, None
    """int"""
    edge_color = None
    tick = None

    needSortDraw = None

    """ check if an another world is empty"""
    isAnotherWorldEmpty = False

    """ contains the winner player """
    worldWinner_player  = None

    def load(self):
        self.elements = []
        self.width = 12
        self.height = self.width*24//16
        self.edge_color = random.choice([color.RED, color.GREEN, color.BLUE, color.YELLOW, color.PURPLE, color.ORANGE, color.WHITE])
        self.tick =  0
        self.needSortDraw   = False
        self.isAlreadyEmpty = False
        self.launch         = None

    def is_rotating(self):
        for element in self.elements:
            if type(element) == ball and element.id == ball_id_connector:
                if element.connector_rotation_add.radian > 0 :
                    return True
                return False

    def get_elements(self):
        return self.elements

    def unload(self):
        for i in range(len(self.elements)):
            self.elements[i].unload()

    def update(self):
        self.tick+=1
        # 'for i in range' loop and not 'for e in elements' loop because new element can be added at the end of elements
        if self.isEmptyWorld():
           for i in range(len(self.elements)):
               if(type(self.elements[i]) is ball and self.elements[i].state != STATE_FALLING):
                    self.elements[i].setFalling() 
        
        for i in range(len(self.elements)):
            self.elements[i].update()
        self.elements =  [x for x in self.elements if x.delete == False]

        if(self.needSortDraw):
            self.needSortDraw = False
            self.elements.sort(key = lambda e: e.importanceDraw)
    
    def draw(self):
        # Check if the world is empty and if so, check if it is the first 
        # if it is the first : there is a win (print a win message)
        if(self.isEmptyWorld() and not(world.isAnotherWorldEmpty) or self.isAlreadyEmpty):
            world.isAnotherWorldEmpty = True
            player_first_empty = players[players.index(self.launch.players_owner)]
            world.worldWinner_player = player_first_empty
            """ if there is a winner, change the scene"""
            scene.change(scenes.win.Win())
        
        s = 2/cam.zoom
        c = self.edge_color
        cam.draw_line(vec2(0,0), vec2(self.width,0), c, s)
        cam.draw_line(vec2(0,self.height), vec2(self.width,self.height), c, s)
        cam.draw_line(vec2(0,0), vec2(0,self.height), c, s)
        cam.draw_line(vec2(self.width,0), vec2(self.width,self.height), c, s)

        for e in self.elements:
            e.draw()
        #cam.draw_font(vec2(self.width/2,self.height*0.6), "world", size=fonts.normal/cam.zoom)

    def add(self, element):
        if(element not in self.elements):
            self.elements.append(element)
            element.world = self
            element.delete = False

    def __iter__(self):
        return iter(self.elements)

    def isEmptyWorld(self):
        balls = [b for b in self if type(b) is ball and b.state == STATE_STATIC]
        if(not(self.isAlreadyEmpty) and len(balls) - 1 == 0):
            if(not(world.isAnotherWorldEmpty)):
                self.isAlreadyEmpty = True
        return len(balls) - 1 == 0 and self.isAlreadyEmpty

    def removeBallsNotConnected(self):
        balls = [b for b in self if type(b) is ball and b.state == STATE_STATIC]
        for b in balls:
            b.isConnected = False

        connectors = [b for b in balls if b.id == ball_id_connector]
        for c in connectors:
            self.markBallAsConnected(c)

        unconnected = [b for b in balls if b.isConnected == False]
        for u in unconnected:
            u.setFalling()
            #u.delete = True
            
    def markBallAsConnected(self, b):
        if(b.isConnected or b.delete):
            return
        b.isConnected = True
        for n in [b for b in b.get_nearest_elements(epsilon) if type(b) is ball and b.state == STATE_STATIC]:
            self.markBallAsConnected(n)
    
    def sortDraw(self):
        self.needSortDraw = True

    def default(index, connectedPlayer):
        cam.center
        w = world()
        w.load()
        h = sqrt(3)/2 # haueteur d'un triangle équilatéral (espacement entre 2 balles sur les y)
        for j in range(int(floor(w.height/h*0.5))):
            xoffset = j%2*0.5 # decale une rangée sur deux
            for i in range(w.width-j%2): #retire une balle sur une rangée sur deux
                if(random.randint(0,100) >= 17+9):
                    ball().preload(w).with_pos(vec2(0.5+i+xoffset, 0.5+h*j)).load()# preload(<world>) en premier, .load() en dernier !


        l = len(w.elements)
        easyCoef = 3 #from 0 to infinite
        for _ in range(int(l*easyCoef)):
            w.elements[random.randint(0, l-1)].absorbNeighborsColor()

        w.elements.sort(key = lambda b: b.pos.y+abs(w.width/2-b.pos.x))
        
        if(len(w.elements) > 0):
            w.elements[0].id = ball_id_connector 
        w.removeBallsNotConnected()

        l = launcher().preload(w).with_pos(vec2(w.width/2, w.height*0.9))
        l.with_player(connectedPlayer[index])
        #l.with_player(the_player.players[random.randint(0, the_player.MAX_PLAYER-1)])
        w.launch = l
        l.load()
        return w

