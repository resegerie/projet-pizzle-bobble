from maths import *
from content_manager import *
import level.the_world as the_world
from interface import *
from players import *

class the_level:
    worlds = None

    def load(self):
        self.worlds = []
        pass
        
    def unload(self):
        for w in self.worlds:
            w.unload()

    def update(self):
        for w in self.worlds:
            w.update()

    def draw(self):
        width  = game.width
        height = game.height*0.9
        yoffset = (game.height-height)/game.height
        
        worldPixelAvgWithMargin = vec2(width/len(self.worlds),height)
        worldPixelAvgNoMargin = vec2(worldPixelAvgWithMargin.x*0.95, worldPixelAvgWithMargin.y)

        cam.push()
        for i in range(len(self.worlds)):
            world = self.worlds[i]

            ratioWorld = world.width/world.height
            ratioX = world.width  / worldPixelAvgNoMargin.x
            ratioY = world.height / worldPixelAvgNoMargin.y

            worldPixelArea = vec2(worldPixelAvgNoMargin.y*ratioWorld, worldPixelAvgNoMargin.y) if(ratioY > ratioX) else vec2(worldPixelAvgNoMargin.x, worldPixelAvgNoMargin.x*ratioWorld)
            #avatar = static_avatar()
            #avatar.set_avatar(i)
            #players.append(Player(f"Player {i}", avatar, i))
            #world.set_player(players[i]) #crash

            cam.zoom = worldPixelArea.x/world.width
            cam.center = hvec2()
            #cam.pos = hvec2(-i, 0, 0, 0)/cam.zoom
            cam.pos = hvec2(((-i*worldPixelAvgWithMargin.x-(worldPixelAvgWithMargin.x-worldPixelArea.x)/2)/width),0,-yoffset,0)
            cam.update()
            #cam.pos = hvec2(-i/worldPixelArea.x, 0, 0, 0)
            world.draw()
        cam.pop()

    def add(self, world):
        self.worlds.append(world)
        
    def default():
        l = the_level()
        l.load()
        connectedPlayer = [p for p in players if p.type != PLAYER_DISCONNECTED]
        for i in range(len(connectedPlayer)):
            l.add(the_world.world.default(i, connectedPlayer))
        return l
