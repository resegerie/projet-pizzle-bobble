from maths import *
from content_manager import *
import level.the_world

epsilon = 0.05

class abstractElement:
    world = None
    delete = None
    tickSpawn = None

    def load(self):
        pass

    def unload(self):
        pass

    def update(self):
        pass

    def draw(self):
        pass

    def __init__(self):
        pass

    def preload(self, worldOrElement):
        w = worldOrElement if type(worldOrElement) == level.the_world.world else worldOrElement.world
        w.add(self)
        self.tickSpawn = w.tick
        self._importanceDraw = 0
        return self

    _importanceDraw = None #should not be changed
    def get_importance_draw(self): return self._importanceDraw
    def set_importance_draw(self, value):
        self._importanceDraw = value
        self.world.sortDraw()
    importanceDraw = property(get_importance_draw, set_importance_draw) 

    @property
    def tick(self): return self.world.tick

    @property
    def age(self): return self.tick-self.tickSpawn

class element(abstractElement):

    pos = None
    """render of the radius"""
    radius = None

    def with_pos(self, pos):
        self.pos = pos
        return self

    def __init__(self):
        self.pos = vec2(0)
        self.radius = 1

    def get_nearest_elements(self, range):
        r = self.radius
        self.radius+=range
        l = [e for e in self.world if self.collideWith(e)]
        self.radius = r
        return l

    def collideWith(self, otherElement):
        if(self is not otherElement):
            if(self.pos.y+self.radius > otherElement.pos.y-otherElement.radius and self.pos.y-self.radius < otherElement.pos.y+otherElement.radius and self.pos.x+self.radius > otherElement.pos.x-otherElement.radius and self.pos.x-self.radius < otherElement.pos.x+otherElement.radius):
                if((self.pos-otherElement.pos).length <= self.radius+otherElement.radius):
                    return True

        return False

