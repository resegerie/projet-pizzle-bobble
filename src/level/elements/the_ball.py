
import random
from maths import *
from content_manager import *
from interface import *
from level.the_element import element, epsilon
import time

__is_loaded = False
max_id = 6 # [0, max_id] #all the ball color
ball_id_connector = max_id + 1 #special

anim = None
"""[animation]"""

STATE_STATIC   = 0
STATE_MOVING   = 1
STATE_LAUNCHER = 2
STATE_FALLING  = 3 #animation before delete

coef_cheating = 1.2

class ball(element):
    id = None
    state = None
    _structure = None

    state_moving_direction = None
    """vec2 (contains angle and speed)"""

    connector_rotation_add = None
    """angle"""
    rotation = None
    tickMoving = None

    shootedByPlayer = None
    """player, can be None"""

    def load(self):
        static_load()
        self.id = random.randint(0, max_id)
        self.state = STATE_STATIC
        self.radius = 0.5
        self.state_moving_target_pos = None
        nbAngle = 6
        AngleOffset = 0
        self._structure = [degree(360/nbAngle*(i+AngleOffset)) for i in range(nbAngle)]
        self.connector_rotation_add = degree(random.randint(0, 1)*random.randint(0, 1)*0.25)
        self.rotation = degree(0)
        self.tickMoving = 0
        self.shootedByPlayer = None

    def movingBallBecomeStatic(self):
        self.state = STATE_STATIC
        r = self.radius
        self.radius *= coef_cheating #cheating a bit
        haveDeletedBall = False
        for b in self.world:
            if((type(b) is not ball) or b.state != STATE_STATIC or b is self):
                continue
            if(self.collideWith(b)):
                self.rotation = b.rotation
                if(b.id == self.id):
                    b.touchedByMovingBall()
                    haveDeletedBall = True

        if(haveDeletedBall):
            self.world.removeBallsNotConnected()
            self.player_animation_great()
        else:
            self.player_animation_hit()
        self.radius = r

    def touchedByMovingBall(self):
        if(self.delete):
            return
        self.setFalling()
        #self.delete = True
        self.radius *= coef_cheating #cheating a bit

        for b in self.world:
            if(b.delete == True or (type(b) is not ball) or b.state != STATE_STATIC or b.id != self.id or self.collideWith(b) == False):
                continue
            b.touchedByMovingBall()

    def rotateBalls(self, angleAdd):
        if(angleAdd.radian == 0): return

        self.rotation += angleAdd
        for b in self.world:
            if((type(b) is not ball) or b.state != STATE_STATIC or self == b):
                continue
            dif = b.pos-self.pos
            dif.angle = dif.angle+angleAdd
            b.pos = self.pos+dif
            b.rotation += angleAdd


    def update(self):
        if(self.state == STATE_STATIC):
            if(self.id != ball_id_connector):
                return
            
            self.rotateBalls(self.connector_rotation_add)
        elif(self.state == STATE_MOVING):
            self.tickMoving += 1
            if(self.tickMoving > game.UPS*16):
                self.setFalling()
                for i in range(3):
                    self.player_animation_dontDoThat()
            self.update_moving_state()
        elif(self.state == STATE_FALLING):
            self.pos += self.speed
            self.speed.x *= 0.99
            self.speed.y = min((self.speed.y+0.003), 0.5)
            if(self.age>5*60):
                self.delete = True

    def checkBorderCollision(self, state_moving_direction):
        """wall collision"""
        nbCollision = 0
        if(self.pos.x-self.radius < 0):
            self.pos.x = self.radius
            state_moving_direction.x = -state_moving_direction.x
            nbCollision+=1
        elif(self.pos.x+self.radius > self.world.width):
            self.pos.x = self.world.width-self.radius
            state_moving_direction.x = -state_moving_direction.x
            nbCollision+=1

        if(self.pos.y-self.radius < 0):
            self.pos.y = self.radius
            state_moving_direction.y = -state_moving_direction.y
            nbCollision+=1
        elif(self.pos.y+self.radius > self.world.height):
            self.pos.y = self.world.height-self.radius
            state_moving_direction.y = -state_moving_direction.y
            nbCollision+=1
        return nbCollision, state_moving_direction

    def find_my_place_inside_structure(self, otherBall):
        posAvailable = []
        oldPos = self.pos
        for a in otherBall.structure:
            add = vec2.from_angle(a).with_length(self.radius+otherBall.radius)
            self.pos = otherBall.pos+add
            if(self.checkBorderCollision(self.state_moving_direction)[0] == 0):
                e = self.get_nearest_elements(-epsilon)
                if(len(e) == 0):
                    posAvailable.append(self.pos)

        bestPos = oldPos
        low_score = 10000000 #trying to minimize
        for pos in posAvailable:
            l = (oldPos-pos).length
            if(l<low_score):
                bestPos = pos
                low_score = l
        self.pos = oldPos
        return bestPos
        #self.movingBallBecomeStatic()

    def absorbNeighborsColor(self):
        n = [e for e in self.get_nearest_elements(epsilon) if type(e) == ball]
        if(len(n) > 0):
            self.id = n[random.randint(0, len(n)-1)].id #copy a neighbord ball color

    def update_moving_state(self):
        self.pos += self.state_moving_direction
        self.state_moving_direction = self.checkBorderCollision(self.state_moving_direction)[1]
        #if(self.pos.y+self.radius>0) #top wall collision #game meachnic : make the top wall sticky (side wall can be sticky too)

        for b in self.world:
            if((type(b) is not ball) or b.state != STATE_STATIC):
                continue
            if(self.collideWith(b)):
                self.pos = self.find_my_place_inside_structure(b)
                self.movingBallBecomeStatic()
                return

    def get_will_colid_with_same_color(self, state_moving_direction):
        pos = self.pos
        tickReachCollision = 0
        balls = self.world.get_elements()
        rotate = self.world.is_rotating() and game.do_detect_rotation
             
        if rotate: 
            ballsPos = []
            for i in balls:
                if(type(i) == ball):
                    ballsPos.append((i.pos, i.rotation.radian))
                else :
                    ballsPos.append((0,0))
                    
        state_moving_direction *= game.precision_CPU
        precision = game.precision_CPU
        for i in range(int(game.UPS*2.5/game.precision_CPU)):
            
            old_pos = self.pos
            tickReachCollision += precision
            self.pos += state_moving_direction
            state_moving_direction = self.checkBorderCollision(state_moving_direction)[1]

            for b in balls:
                if((type(b) is not ball) or b.state != STATE_STATIC):
                    continue
                if(self.collideWith(b)):
                    
                    if(precision != 1):
                        tickReachCollision -= precision
                        state_moving_direction /= precision
                        self.pos = old_pos
                        precision = 1
                    else:
                        self.pos = pos
                        if rotate:
                            for i in range(len(balls)):
                                if type(balls[i]) is not ball : continue
                                balls[i].pos = ballsPos[i][0]
                                balls[i].rotation = radian(ballsPos[i][1])
                        if(b.id == self.id):
                            return (True, tickReachCollision)
                        return (False, -1)
            if rotate:
                for b in balls:
                    if(type(b) == ball and b.id == ball_id_connector):
                        b.rotateBalls(b.connector_rotation_add*precision)
                        break
        self.pos = pos
        if rotate:
            for i in range(len(balls)):
                if type(balls[i]) is not ball : continue
                balls[i].pos = ballsPos[i][0]
                balls[i].rotation = radian(ballsPos[i][1])
        return (False, -1)

    @property
    def structure(self):
        return [a+self.rotation for a in self._structure]

    def draw(self):
        """
        for a in self.structure:
            cam.draw_line(self.pos, self.pos+vec2.from_angle(a).with_length(self.radius+0.25), color.WHITE, width=1/cam.zoom)
        """

        i = (self.tick//6+int(self.pos.x+self.pos.y))%anim[self.id].length
        cam.draw_sprite(anim[self.id].get(i), self.pos, vec2(2*self.radius))

    def setFalling(self):
        self.state = STATE_FALLING
        self.tickSpawn = self.tick
        sx = 0.05
        sy = -0.1
        self.importanceDraw = 10
        self.speed = vec2(random.random() * sx*2 -sx, random.random()*sy)

    def player_animation_hit(self):
        game.audio.play_sound("bubble_hit.mp3", 0.3)
        if(self.shootedByPlayer != None):
            pa = self.shootedByPlayer.active_avatar
            pa.playAnimationNow(pa.static_avatar.hitAnimation)

    def player_animation_great(self):
        game.audio.play_sound("bubble_pop.mp3", 0.5)
        if(self.shootedByPlayer != None):
            pa = self.shootedByPlayer.active_avatar
            pa.playAnimationNow(pa.static_avatar.greatAnimation)

    def player_animation_dontDoThat(self):
        if(self.shootedByPlayer != None):
            pa = self.shootedByPlayer.active_avatar
            pa.playAnimationNow(pa.static_avatar.dontDoThatAnimation)

def static_load(): #loaded just once
    global anim, __is_loaded
    if(__is_loaded):
        return
    __is_loaded = True
    anim = [animation().from_rectangles(get_image("cheese_ball"), 10, 8, 8+48*i, 32, 32, 16) for i in range(max_id+2)]
