
import random, input
import re
from maths import *
from content_manager import *
from interface import *
from level.the_element import element
from level.elements.the_ball import ball, STATE_MOVING, STATE_STATIC, STATE_LAUNCHER, ball_id_connector, max_id
from players import *
import time

tickReload = 60 #in tick 

class launcher(element):
    angle = None
    ball = None
    players_owner = None
    tickLastshoot = None
    
    """CPU"""
    cpu_moove = None
    cpu_offset_tick = None
    
    @property
    def tickSinceLastShoot(self): return self.tick-self.tickLastshoot

    def load(self):
        self.angle = degree(270)
        self.reloadBall()
        self.cpu_moove = 1
        self.cpu_offset_tick = random.randint(0, 100)
        self.tickLastshoot = 0
        assert self.players_owner != None, "missing players_owner in launcher. players_owner can be not connected if needed"
    

    def input_get_turn(self):
        """return -1, 0 or 1"""
        if(self.players_owner.type == PLAYER_HUMAN):
            add = 0
            if input.isKeyPressed(self.players_owner.keyRight): #todo replace by player input
                add += 1
            elif input.isKeyPressed(self.players_owner.keyLeft):
                add -= 1
            return add    
        else:
            add = 0
            if((self.angle.degree%360)>190 and (self.angle.degree%360)<195):
                self.cpu_moove = +1
            elif((self.angle.degree%360)>340 and (self.angle.degree%360)<345):
                self.cpu_moove = -1
            add += self.cpu_moove 
            return add
    
    def is_ball_left(self):
        for i in self.world.elements:
            if type(i) == ball and i.state == STATE_STATIC and i.id != max_id+1:
                return True
        return False
    
    
    def cpu_shoot(self):
        #game.Performance_FPS < 55 or 
        #(self.tick + self.cpu_offset_tick) % 6 != 0 or 
        if((self.tick + self.cpu_offset_tick) % 2 != 0 or (not self.is_ball_left())): 
            return False
        result = self.ball.get_will_colid_with_same_color(self.get_vec2_ball_speed())
        if (result[0] or self.tickSinceLastShoot >= tickReload+6*60):
            return True
        return False

    def get_vec2_ball_speed(self): return vec2.from_angle(self.angle).with_length(9/60)

    def input_get_shoot(self):
        if(self.players_owner.type == PLAYER_HUMAN):
            return input.isKeyPullUp(self.players_owner.keyUp)
        else:
            return self.cpu_shoot()

    def update(self):
        if(self.tickSinceLastShoot > tickReload and self.input_get_shoot()):
                # Sound for the ball here
                game.audio.play_sound("bubble_sound.mp3", 0.3)
                self.tickLastshoot = self.tick
                self.ball.state = STATE_MOVING
                self.ball.state_moving_direction = self.get_vec2_ball_speed()
                pa = self.players_owner.active_avatar
                pa.playAnimationNow(pa.static_avatar.shootAnimation)
                self.reloadBall()

        self.angle = degree(self.angle.degree+self.input_get_turn()/60*60)
    
    def draw(self):
        end = self.pos+vec2.from_angle(self.angle).with_length(2)
        start = self.pos+vec2.from_angle(self.angle).with_length(-0.75)
        
        radius = 0.5
        self.ball.radius =radius if self.tickSinceLastShoot > tickReload else radius*(self.tickSinceLastShoot/tickReload)**0.8

        cam.draw_line(start, end, color.WHITE, 2/cam.zoom)
        cam.draw_line(end, end+vec2.from_angle(degree(180+self.angle.degree+20)).with_length(1), color.WHITE, 4/cam.zoom)
        cam.draw_line(end, end+vec2.from_angle(degree(180+self.angle.degree-20)).with_length(1), color.WHITE, 4/cam.zoom)
        cam.draw_line(start, start+vec2.from_angle(degree(180+self.angle.degree+40)).with_length(0.5), color.WHITE, 4/cam.zoom)
        cam.draw_line(start, start+vec2.from_angle(degree(180+self.angle.degree-40)).with_length(0.5), color.WHITE, 4/cam.zoom)
        cam.draw_ellipse(self.pos, vec2(0.5), color.WHITE)


        if(self.players_owner.type != PLAYER_DISCONNECTED):
            self.players_owner.draw()

            if(self.players_owner.type == PLAYER_HUMAN):
                self.players_owner.draw_control()

    def with_player(self, players_owner):
        self.players_owner = players_owner
        if(self.players_owner is not None):
            a = self.players_owner.active_avatar
            a.hitbox = vec2(4)
            a.pos = self.pos+vec2(2,a.hitbox.y/4)
        return self

    def reloadBall(self):
        ids =  list({b.id for b in self.world if type(b) == ball and b.state == STATE_STATIC})
        ids.remove(ball_id_connector) # can't be launch

        self.ball = ball()
        self.ball.preload(self).with_pos(self.pos)
        self.ball.load()
        if(len(ids)>0):
            self.ball.id = random.choice(ids)
        self.ball.state = STATE_LAUNCHER
        self.ball.shootedByPlayer = self.players_owner