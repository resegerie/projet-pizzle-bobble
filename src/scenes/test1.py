import scene, pygame, scenes.enJeu, input
from pygame.locals import *
from interface import *
from maths import *
from content_manager import *

class EcranTitre(scene.Scene):
    testIcon = None

    def load(self):
        print("ecran titre du jeu")
        self.testIcon = get_image("icon.png")

    def update(self):
        #cam.pos.x += 1
        if input.isKeyPullUp(pygame.K_SPACE):
            scene.change(scenes.enJeu.EnJeu())

    def draw(self):
        #cam.zoom = (cam.zoom+0.001)*1.002 #todo marche pas encore
        #cam.center

        cam.draw_line(hvec2(0,0.05,0.25,0), hvec2(0,0.5,0.75,0), width=2)

        #par défaut, la fonction le rectangle est centré en son milieu (voir l'argument center qui est un vec2 = vec2(0.5) par défaut)
        cam.draw_rect(vec2(32,32), vec2(48), color.RED) # affiche à une position et taille fixe (vec2)

        # centré à 25% sur les X, 50% sur les Y
        cam.draw_rect(hvec2(0.25, 0, 0.5, 0), vec2(48), color.BLUE) # affiche à une position relative à la fenetre et à un taille fixe
        

        #position relative et taille relative
        margin = 0.05 # 5 % de marge basé sur l'axe des Y
        cam.draw_rect(hvec2(1, -margin, 1-margin, 0), hvec2(0, 0.1, 0.1, 0), color.GREEN, center=vec2(1))
        cam.draw_ellipse(hvec2(1,0,0,0), hvec2(0, 0.1, 0.1, 0), color.rgb(color.ORANGE.r,color.ORANGE.g,color.ORANGE.b, 255), center=vec2(1,0))
        cam.draw_font(hvec2().with_pourcent(0.5, 0.2), "Hello World !!! 123456789", center=fonts.center, colorBackground= color.DARK_GREEN, colorForeground=color.YELLOW)


        posCenter = hvec2(0.5, 0, 0.5, 0)
        sizeHalfY = hvec2(0, 0.5, 0.5, 0)
        cam.draw_image(self.testIcon, posCenter, sizeHalfY, vec2rectangleSizePx=vec2(32), vec2rectangleOriginPx=vec2(32, 0)) #, flipX=False)


