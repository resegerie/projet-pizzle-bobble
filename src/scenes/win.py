from cgitb import text
from platform import platform
from turtle import update
import scene 
from pygame.locals import *
from pygame import mixer
from interface import *
from maths import *
from content_manager import *
import time
from maths import *
from players import *
import players.the_player as p
import level.the_world as w


""" win scene for winners """
class Win(scene.Scene):

    def load(self):
        self.start_menu      = self.ui.add(uiText("Press Echap.").with_pos( hvec2(0.5, 0, 0.6, 0) ))
        self.letter_y_target = 0.05
        titleName            = "!! " + w.world.worldWinner_player.active_avatar.static_avatar.name + " WIN !!"
        last                 = uiText(titleName).with_pos(hvec2(0.5, 0, 1, 0)).with_center(vec2(0.5,0)).with_size(fonts.enormous)
        last.pos            -= last.hitbox*hvec2(1.5, 1.5, 0, 0)
        self.letter          = []
        i                    = 0
        for l in titleName:
            last = self.ui.add(uiText(l).relative(last, vec2(1, 0)).with_center(vec2(0, 0)).with_size(fonts.enormous))
            self.letter.append(last)
            last.pos.y += i
            i+=1
        game.audio.play_sound("win_sound.mp3", 6)

    def displayPlayer(self, player):
        hitbox = hvec2(0, 0.4, 0.6, 0)
        pos = hvec2(0.5, 0, 0.5, 0)
        player.active_avatar.hitbox = vec2(hitbox.to_vec2().x*0.7)
        player.active_avatar.pos = pos.to_vec2()+vec2(0, w.world.worldWinner_player.active_avatar.hitbox.y*0.2)
        player.draw()
        # victory pose ?
        #player.active_avatar.playAnimation(player.active_avatar.static_avatar.greatAnimation)
        #player.active_avatar.playAnimation(player.active_avatar.static_avatar.greatAnimation)

    def draw(self):
        self.displayPlayer(w.world.worldWinner_player)
        i = 0
        t = (time.time()*100)//5%20

        for l in self.letter:
            i+=1
            a = degree((time.time())*250+i*180)
            l.pos.y = lerp(l.pos.y, self.letter_y_target+a.cos*0.01, 0.1)
            
            targetColor = color.WHITE if t==i or t==i-1 else color.YELLOW
            l.color = color.lerp(l.color, targetColor, 0.8 if targetColor == color.WHITE else 0.1)
    
    
            
