import scene, pygame, scenes.enJeu, input
from pygame.locals import *
from pygame import mixer
from interface import *
from maths import *
from content_manager import *
import time
import players
import players.the_static_avatar as the_static_avatar
import players.the_active_avatar as the_active_avatar

class ChoosePerso(scene.Scene):
    
    def displayJoueur(self, index):
        p = players.players[index]
        pa = p.active_avatar
        ps = pa.static_avatar
        hitbox = hvec2(0, 0.4, 0.6, 0)
        spacing = hvec2(hitbox.x, hitbox.xBasedy+0.1, 0, 0)
        pos = hvec2(0.5, 0, 0.5, 0)+spacing*(index-(players.MAX_PLAYER-1)/2)
        cam.draw_rect(pos, hitbox, color.lerp(ps.color, color.BLACK, 0.8))
        cam.draw_font(pos-hvec2(0,0, hitbox.y*0.4, 0), ps.name, colorForeground=ps.color)
        pa.hitbox = vec2(hitbox.to_vec2().x*0.7)
        pa.pos = pos.to_vec2()+vec2(0, pa.hitbox.y*0.2)
        p.draw()
        p.draw_control_at(pos+hvec2(0, 0, hitbox.y*0.25, 0))

        cam.draw_font(pos-hvec2(0,0, -hitbox.y*0.45, 0), p.type_to_string(), colorForeground=color.GRAY)

    def updateJoueur(self, index):
        p = players.players[index]
        pa = p.active_avatar
        ps = pa.static_avatar

        indexAvatar = ps.index
        if(input.isKeyPullDown(p.keyRight)):
            indexAvatar +=1
        elif(input.isKeyPullDown(p.keyLeft)):
            indexAvatar -=1

        if(input.isKeyPullDown(p.keyUp)):
            p.type = players.PLAYER_HUMAN if p.type != players.PLAYER_DISCONNECTED else players.PLAYER_CPU 
        elif(input.isKeyPullDown(p.keyDown)):
            p.type = players.PLAYER_DISCONNECTED if p.type != players.PLAYER_HUMAN else players.PLAYER_CPU

        if(indexAvatar != ps.index):
            p.active_avatar = the_active_avatar.active_avatar(the_static_avatar.static_avatars[(indexAvatar+the_static_avatar.MAX_AVATAR)%the_static_avatar.MAX_AVATAR])

    def load(self):
        self.title = self.ui.add(uiText("Choisissez vos joueurs :").with_pos(hvec2(0.5, 0, 0.05, 0)).with_center(vec2(0.5,0)))
        self.space = self.ui.add(uiText("Espace pour continuer").with_pos(hvec2(0.5, 0, 1-0.05, 0)).with_center(vec2(0.5,1)))

    def update(self):
        for i in range(players.MAX_PLAYER):
            self.updateJoueur(i)

        if input.isKeyPullUp(pygame.K_SPACE):
            if(len([p for p in players.players if p.type != players.PLAYER_DISCONNECTED]) != 0):
                scene.change(scenes.enJeu.EnJeu())
            else:
                self.space.color = color.RED

    def draw(self):
        self.space.color = color.lerp(self.space.color, color.WHITE, 0.05)

        for i in range(players.MAX_PLAYER):
            self.displayJoueur(i)
        # Menu options

        #l.pos.y = lerp(l.pos.y, self.letter_y_target-0.01*a.cos*max(1,(25+i*40)/((self.tick_elapsed)/30+1)**2), 0.01)
        #self.title.pos = self.title_targetPos+hvec2(0,0,0.25,0)*(-a.cos*max(0.05,5/(self.tick_elapsed/10+1)))
        #cam.zoom = (cam.zoom+0.001)*1.002 #todo marche pas encore
        #cam.center

        #cam.draw_line(hvec2(0,0.05,0.25,0), hvec2(0,0.5,0.75,0), width=2)

        #par défaut, la fonction le rectangle est centré en son milieu (voir l'argument center qui est un vec2 = vec2(0.5) par défaut)
        #cam.draw_rect(vec2(32,32), vec2(48), color.RED) # affiche à une position et taille fixe (vec2)

        # centré à 25% sur les X, 50% sur les Y
        #cam.draw_rect(hvec2(0.25, 0, 0.5, 0), vec2(48), color.BLUE) # affiche à une position relative à la fenetre et à un taille fixe
        

        #position relative et taille relative
        #margin = 0.05 # 5 % de marge basé sur l'axe des Y
        #cam.draw_rect(hvec2(1, -margin, 1-margin, 0), hvec2(0, 0.1, 0.1, 0), color.GREEN, center=vec2(1))
        #cam.draw_ellipse(hvec2(1,0,0,0), hvec2(0, 0.1, 0.1, 0), color.rgb(color.ORANGE.r,color.ORANGE.g,color.ORANGE.b, 255), center=vec2(1,0))
        #cam.draw_font(hvec2().with_pourcent(0.5, 0.2), "Hello World !!! 123456789", center=fonts.center, colorBackground= color.DARK_GREEN, colorForeground=color.YELLOW)


        #vposCenter = hvec2(0.5, 0, 0.5, 0)
        #sizeHalfY = hvec2(0, 0.5, 0.5, 0)
        #cam.draw_image(self.testIcon, posCenter, sizeHalfY, vec2rectangleSizePx=vec2(32), vec2rectangleOriginPx=vec2(32, 0)) #, flipX=False)

