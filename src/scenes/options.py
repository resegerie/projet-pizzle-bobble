from cgitb import text
from turtle import update
import scene, pygame, scenes.ecranTitre, game, input
from pygame.locals import *
from pygame import mixer
from interface import *
from maths import *
from content_manager import *
import time
import players.the_player as p

class Options(scene.Scene):
    letter_y_target = None
    letter = None

    cursor = None
    """uiImage"""
    
    choices = None
    """array of uiText"""
    choiceIndex = None
    
    play, option, quit = (None, None, None)
    """uiText"""

    audio = None
    """music"""
    
    """CPU"""
    PRECISION_MAX = 20

    def load(self):
        print("OPTIONS")
        self.letter_y_target = 0.05

        titleName = "Options"
        offset    = hvec2(0,0,0.05,0)
        self.file_path = "option.pizz"
        self.play                   = self.ui.add(uiText(         "Menu"        ).with_pos( hvec2(0.5, 0, 0.4, 0) ))
        self.volume                 = self.ui.add(uiText(        "Volume"       ).under(     self.play, offset    ))
        self.key                    = self.ui.add(uiText(     "> Touches :"     ).under(    self.volume, offset   ))
        self.precision_CPU          = self.ui.add(uiText(   "Precision CPU :"   ).under(      self.key, offset    ))
        self.do_detect_rotation_CPU = self.ui.add(uiText("Detect rotation CPU :").under(self.precision_CPU, offset))

        self.choices = [self.play, self.volume, self.key, self.precision_CPU, self.do_detect_rotation_CPU]
        self.choiceIndex = 0

        """ just for print """
        self.keyChoices= ["Flèches directionnelles", "DQSZ : Gaucher Gamer", "Mode JGHY"]
        self.keyGame = [(pygame.K_RIGHT, pygame.K_LEFT, pygame.K_DOWN, pygame.K_UP),
                        (pygame.K_d, pygame.K_q, pygame.K_s, pygame.K_z),
                        (pygame.K_j, pygame.K_g, pygame.K_h, pygame.K_y) ]

        self.choiceIndexKey = 0
        self.keyOn = self.keyChoices[self.choiceIndexKey]

        """ current / default key in game"""
        self.keyRight = pygame.K_RIGHT
        self.keyLeft  = pygame.K_LEFT
        self.keyDown  = pygame.K_DOWN
        self.keyUp    = pygame.K_UP

        self.keyOnGame = [self.keyRight, self.keyLeft, self.keyDown, self.keyUp]

        self.isOnVolume            = False
        self.isOnKey               = False
        self.isOnPrecision         = False
        self.isOnDoDetectRotation  = False

        self.cursor = self.ui.add(uiImage("cheese_cursor").with_hitbox(hvec2(0, 0.1, 0.1, 0)))
        self.audio  = game.audio

        last = uiText(titleName).with_pos(hvec2(0.5, 0, 1, 0)).with_center(vec2(0.5,0)).with_size(fonts.enormous)
        last.pos -= last.hitbox*hvec2(1.5, 1.5, 0, 0)
        self.letter = []
        i=0

        for l in titleName:
            last = self.ui.add(uiText(l).relative(last, vec2(1, 0)).with_center(vec2(0, 0)).with_size(fonts.enormous))
            self.letter.append(last)
            last.pos.y += i
            i+=1
        self.update_with_save_file()
            
    """ update the vol./key/precision and rotation CPU
        with the save file "option.pizz" 
        - modify the key settings for players with
        their preferences """
    def update_with_save_file(self):
        opt = []
        with open(self.file_path, "r") as f:
            for data in f:
                opt.append(data.strip())
        if(len(opt) != 0):
            self.choiceIndexKey     =    int (opt[1])
            game.precision_CPU      =    int (opt[2])
            game.do_detect_rotation =    opt[3]
            self.audio.set_volume(float(opt[0]))

            self.keyOn              = self.keyChoices[self.choiceIndexKey]
            self.keyRight           = self.keyGame[self.choiceIndexKey][0]
            self.keyLeft            = self.keyGame[self.choiceIndexKey][1]
            self.keyDown            = self.keyGame[self.choiceIndexKey][2]
            self.keyUp              = self.keyGame[self.choiceIndexKey][3]
            self.update_key()
             
    def update(self):
        if input.isKeyPullUp(pygame.K_SPACE):
            textConfig = ""
            textConfig += str(self.audio.get_volume())   + "\n"
            textConfig += str(self.choiceIndexKey)       + "\n"
            textConfig += str(game.precision_CPU)        + "\n"
            textConfig += str(game.do_detect_rotation)
            file        = open(  self.file_path, "w"   )
            file.write(textConfig)
            file.close(          )
            scene.change(scenes.ecranTitre.EcranTitre())
        if(input.isKeyPullUp(pygame.K_LEFT) and self.audio.get_volume() >= 0.09 and self.isOnVolume):
            self.audio.set_volume(self.audio.get_volume() - 0.1)
        elif(input.isKeyPullUp(pygame.K_RIGHT) and self.audio.get_volume() < 2 and self.isOnVolume):
            self.audio.set_volume(self.audio.get_volume() + 0.1)
        elif(input.isKeyPullUp(pygame.K_RIGHT) and self.isOnKey):

            self.choiceIndexKey = (self.choiceIndexKey + 1) % len(self.keyChoices)

            self.keyOn = self.keyChoices[self.choiceIndexKey]

            self.keyRight = self.keyGame[self.choiceIndexKey][0]
            self.keyLeft  = self.keyGame[self.choiceIndexKey][1]
            self.keyDown  = self.keyGame[self.choiceIndexKey][2]
            self.keyUp    = self.keyGame[self.choiceIndexKey][3]
            
            self.update_key()
        elif(input.isKeyPullUp(pygame.K_RIGHT) and self.isOnPrecision):
            game.precision_CPU -= 1
            if(game.precision_CPU < 1) : game.precision_CPU = 1
            
        elif(input.isKeyPullUp(pygame.K_LEFT) and self.isOnPrecision):
            game.precision_CPU+= 1
            if(game.precision_CPU > self.PRECISION_MAX) : game.precision_CPU = self.PRECISION_MAX
        
        elif((input.isKeyPullUp(pygame.K_LEFT) or input.isKeyPullUp(pygame.K_RIGHT)) and self.isOnDoDetectRotation):
            game.do_detect_rotation = not game.do_detect_rotation
            

    # Update the cursor based on menu state
    def move_cursor(self):
        #cursor_sound = mixer.Sound(get_content_path("music", "sound_cursor.wav"))
        #cursor_sound.set_volume(0.8)
        add=0   
        if input.isKeyPullUp(pygame.K_DOWN):
            self.audio.play_sound("sound_cursor.wav", 0.8)
            add = 1
        elif input.isKeyPullUp(pygame.K_UP):
            self.audio.play_sound("sound_cursor.wav", 0.8)
            add = -1        
        l = len(self.choices)
        self.choiceIndex = ((self.choiceIndex+add+l)%l)

    def update_music(self):
        return self.audio.get_volume()
    
    def change_key(self):
        p.players[0].keyRight = self.keyRight
        p.players[0].keyLeft  = self.keyLeft
        p.players[0].keyDown  = self.keyDown
        p.players[0].keyUp    = self.keyUp

    def update_key(self):
        if(self.keyRight == pygame.K_RIGHT):
            self.change_key()
            p.players[1].keyRight = pygame.K_j
            p.players[1].keyLeft  = pygame.K_g
            p.players[1].keyDown  = pygame.K_h
            p.players[1].keyUp    = pygame.K_y 

            p.players[2].keyRight = pygame.K_d
            p.players[2].keyLeft  = pygame.K_q
            p.players[2].keyDown  = pygame.K_s
            p.players[2].keyUp    = pygame.K_z

        elif(self.keyRight == pygame.K_d):
            self.change_key()
            p.players[1].keyRight = pygame.K_RIGHT
            p.players[1].keyLeft  = pygame.K_LEFT
            p.players[1].keyDown  = pygame.K_DOWN
            p.players[1].keyUp    = pygame.K_UP 

            p.players[2].keyRight = pygame.K_j
            p.players[2].keyLeft  = pygame.K_g
            p.players[2].keyDown  = pygame.K_h
            p.players[2].keyUp    = pygame.K_y

        elif(self.keyRight == pygame.K_j and self.keyLeft == pygame.K_g):
            self.change_key()
            p.players[1].keyRight = pygame.K_RIGHT
            p.players[1].keyLeft  = pygame.K_LEFT
            p.players[1].keyDown  = pygame.K_DOWN
            p.players[1].keyUp    = pygame.K_UP 

            p.players[2].keyRight = pygame.K_d
            p.players[2].keyLeft  = pygame.K_q
            p.players[2].keyDown  = pygame.K_s
            p.players[2].keyUp    = pygame.K_z

    def draw(self):
        if(self.volume == self.choices[self.choiceIndex]):
            self.isOnVolume = True
            self.isOnKey = False
            cam.draw_font(self.volume.pos + hvec2(0.13,0 ,0, 0), " Vol. : " + str(round(abs(float(self.audio.print_volume()) * 10), 1)))
        elif(self.key == self.choices[self.choiceIndex]):
            cam.draw_font(self.key.pos + hvec2(0.27,0 ,0, 0), str(self.keyOn))
            self.isOnKey = True
            self.isOnVolume = False
        elif(self.precision_CPU == self.choices[self.choiceIndex]):
            cam.draw_font(self.precision_CPU.pos + hvec2(0.15,0 ,0, 0), str(self.PRECISION_MAX+1-game.precision_CPU))
            self.isOnPrecision = True
            self.isOnKey = False
            self.isOnVolume = False
        elif(self.do_detect_rotation_CPU == self.choices[self.choiceIndex]):
            txt = str(game.do_detect_rotation)
            cam.draw_font(self.do_detect_rotation_CPU.pos + hvec2(0.22,0 ,0, 0), txt)
            self.isOnPrecision = False
            self.isOnKey = False
            self.isOnVolume = False
            self.isOnDoDetectRotation = True
        i = 0
        t = (time.time()*100)//5%20
        for l in self.letter:
            i+=1
            a = degree((time.time())*250+i*180)
            l.pos.y = lerp(l.pos.y, self.letter_y_target+a.cos*0.01, 0.1)
            
            targetColor = color.WHITE if t==i or t==i-1 else color.GREEN
            l.color = color.lerp(l.color, targetColor, 0.8 if targetColor == color.WHITE else 0.1)
    
        self.move_cursor()

        for c in self.choices:
            c.with_colorBackground(color.ORANGE if c == self.choices[self.choiceIndex] else None)

        self.cursor.relative(self.choices[self.choiceIndex], vec2(-1,0))
