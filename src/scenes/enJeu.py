import scene, pygame, scenes.enJeu, input
from pygame.locals import *
from interface import *
from maths import *
from content_manager import *
from level.the_level import *

class EnJeu(scene.Scene):

    level = None

    def load(self):
        self.level = the_level.default()

    def unload(self):
        self.level.unload()

    def update(self):
        self.level.update()

    def draw(self):
        cam.draw_font(hvec2(0,0,0,0), " En jeu : (fps:"+str(floor(game.Performance_FPS))+")", center=vec2(0,0))
        self.level.draw()
