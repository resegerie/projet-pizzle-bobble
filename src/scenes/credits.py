import scene, pygame, game, input, scenes.ecranTitre
from pygame.locals import *
from pygame import mixer
from interface import *
from maths import *
from content_manager import *

class Credits(scene.Scene):
    
    def load(self):
        print("credits du jeu")

        l = ["Pizzle Bubble:"]
        
        l.extend(["================= Credits =========", ""])
        
        l.extend(["Music: TheFatRat - Unity", "  www.youtube.com/watch?v=n8X9_MgEdCg"])
        l.extend(["Sound effect: Zapsplat", "  zapsplat.com"])
        l.extend(["Font: ThomasFont7", "  fontstruct.com/fontstructions/show/1709939/pixelhandtest6-2", ""])

        l.extend(["Programming and Sprites:","  TAMAGNAUD Thomas, SEGERIE Reynalde, ROUBILLE Mathis", ""])
        l.extend(["Supervising:", "  Lucas Pastor", ""])

        l.extend(["================= Thank for playing ========="])
        l.extend(["Space to continue","2022"])

        for i in range(len(l)):
            txt = l[i]
            if(len(txt) != 0):
                u = self.ui.add(uiText(txt).with_pos(hvec2(0, 0.05, i*0.05, 0)).with_center(vec2(0,0)))
                if txt[0] == '=':
                    u.color = color.GREEN
                elif txt[0] == ' ':
                    u.color = color.BLUE if i < 9 else color.YELLOW

    
    def update(self):
        if input.isKeyPullUp(pygame.K_SPACE):
            scene.change(scenes.ecranTitre.EcranTitre())

   
    