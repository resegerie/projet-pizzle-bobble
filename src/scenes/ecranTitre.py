import scene, pygame, scenes.enJeu, scenes.options, scenes.choosePerso, scenes.credits, input
from pygame.locals import *
from pygame import mixer
from interface import *
from maths import *
from content_manager import *
import time

class EcranTitre(scene.Scene):
    letter_y_target = None
    letter = None

    cursor = None
    """uiImage"""
    
    choices = None
    """array of uiText"""
    choiceIndex = None
    
    play, option, credit, quit = (None, None, None, None)
    """uiText"""

    def load(self):
        print("ecran titre du jeu")
        self.letter_y_target = 0.05

        titleName = "PiZZle BuBBle"
        offset = hvec2(0,0,0.05,0)

        self.play   = self.ui.add(uiText("Play").with_pos(hvec2(0.5, 0, 0.4, 0)))
        self.option = self.ui.add(uiText("Options").under(self.play, offset))
        self.credit = self.ui.add(uiText("Credits").under(self.option, offset))
        self.quit   = self.ui.add(uiText("Quit").under(self.credit, offset))

        self.choices = [self.play, self.option, self.credit, self.quit]
        self.choiceIndex = 0

        self.cursor   = self.ui.add(uiImage("cheese_cursor").with_hitbox(hvec2(0, 0.1, 0.1, 0)))

        last = uiText(titleName).with_pos(hvec2(0.5, 0, 1, 0)).with_center(vec2(0.5,0)).with_size(fonts.enormous)
        last.pos -= last.hitbox*hvec2(1.5, 1.5, 0, 0)
        self.letter = []
        i=0
        lastMoove = time.time()
        for l in titleName:
            last = self.ui.add(uiText(l).relative(last, vec2(1, 0)).with_center(vec2(0, 0)).with_size(fonts.enormous))
            self.letter.append(last)
            last.pos.y += i
            i+=1
            lastMoove = time.time()
        opt_scene = scenes.options.Options()
        opt_scene.load()
        opt_scene.update_with_save_file()

    def update(self):
        if input.isKeyPullUp(pygame.K_SPACE):
            choice = self.choices[self.choiceIndex]
            if self.play == choice:
                scene.change(scenes.choosePerso.ChoosePerso())
            elif self.option == choice:
                scene.change(scenes.options.Options())
            elif self.credit == choice:
                scene.change(scenes.credits.Credits())
            elif self.quit == choice:
                game.unload()

    # Update the cursor based on menu state
    def move_cursor(self):
        cursor_sound = mixer.Sound(get_content_path("music", "sound_cursor.wav"))
        cursor_sound.set_volume(0.8)
        add=0
        if input.isKeyPullUp(pygame.K_DOWN):
            cursor_sound.play()
            add = 1
        elif input.isKeyPullUp(pygame.K_UP):
            cursor_sound.play()
            add = -1        
        l = len(self.choices)
        self.choiceIndex = (self.choiceIndex+add+l)%l

    def draw(self):
        i = 0
        t = (time.time()*100)//5%20
        for l in self.letter:
            i+=1
            a = degree((time.time())*250+i*180)
            l.pos.y = lerp(l.pos.y, self.letter_y_target+a.cos*0.01, 0.1)
            
            targetColor = color.WHITE if t==i or t==i-1 else color.YELLOW
            l.color = color.lerp(l.color, targetColor, 0.8 if targetColor == color.WHITE else 0.1)
    
        self.move_cursor()

        # Drawing cursor
        #cam.draw_image(self.cursor, hvec2().with_pourcent(self.cursorX, self.cursorY),vec2(50))

        for c in self.choices:
            c.with_colorBackground(color.ORANGE if c == self.choices[self.choiceIndex] else None)

        self.cursor.relative(self.choices[self.choiceIndex], vec2(-1,0))
        # Menu options

        #l.pos.y = lerp(l.pos.y, self.letter_y_target-0.01*a.cos*max(1,(25+i*40)/((self.tick_elapsed)/30+1)**2), 0.01)
        #self.title.pos = self.title_targetPos+hvec2(0,0,0.25,0)*(-a.cos*max(0.05,5/(self.tick_elapsed/10+1)))
        #cam.zoom = (cam.zoom+0.001)*1.002 #todo marche pas encore
        #cam.center

        #cam.draw_line(hvec2(0,0.05,0.25,0), hvec2(0,0.5,0.75,0), width=2)

        #par défaut, la fonction le rectangle est centré en son milieu (voir l'argument center qui est un vec2 = vec2(0.5) par défaut)
        #cam.draw_rect(vec2(32,32), vec2(48), color.RED) # affiche à une position et taille fixe (vec2)

        # centré à 25% sur les X, 50% sur les Y
        #cam.draw_rect(hvec2(0.25, 0, 0.5, 0), vec2(48), color.BLUE) # affiche à une position relative à la fenetre et à un taille fixe
        

        #position relative et taille relative
        #margin = 0.05 # 5 % de marge basé sur l'axe des Y
        #cam.draw_rect(hvec2(1, -margin, 1-margin, 0), hvec2(0, 0.1, 0.1, 0), color.GREEN, center=vec2(1))
        #cam.draw_ellipse(hvec2(1,0,0,0), hvec2(0, 0.1, 0.1, 0), color.rgb(color.ORANGE.r,color.ORANGE.g,color.ORANGE.b, 255), center=vec2(1,0))
        #cam.draw_font(hvec2().with_pourcent(0.5, 0.2), "Hello World !!! 123456789", center=fonts.center, colorBackground= color.DARK_GREEN, colorForeground=color.YELLOW)


        #vposCenter = hvec2(0.5, 0, 0.5, 0)
        #sizeHalfY = hvec2(0, 0.5, 0.5, 0)
        #cam.draw_image(self.testIcon, posCenter, sizeHalfY, vec2rectangleSizePx=vec2(32), vec2rectangleOriginPx=vec2(32, 0)) #, flipX=False)

