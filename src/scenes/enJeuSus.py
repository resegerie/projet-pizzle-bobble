"""
import math
import scene, game, pygame
from pygame.locals import *
from interface import *
import input
from content_manager.spr_arrow import Arrow
#from content_manager.grid_bubble import Grid

# cheese

▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░████████████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                ████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██          ████████        ████████▒▒▒▒▒▒
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██          ██░░░░░░░░██              ████▒▒
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░██        ██░░░░░░░░██                ████
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██░░░░░░██          ████████            ██████  ██
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██  ██████                        ████████        ██
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██          ██████            ██████                ██
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██          ██░░░░░░██    ██████                      ██
▒▒▒▒▒▒▒▒▒▒▒▒▒▒██        ████░░░░░░░░░░████                      ██████▒▒
▒▒▒▒▒▒▒▒▒▒▒▒██          ████░░░░░░░░░░██                      ██░░██▒▒▒▒
▒▒▒▒▒▒▒▒▒▒██      ██████████░░░░░░░░░░██                      ██░░██▒▒▒▒
▒▒▒▒▒▒▒▒██  ██████      ████░░░░░░░░░░██                        ██████▒▒
▒▒▒▒▒▒▒▒████                ██░░░░░░██                              ██▒▒
▒▒▒▒▒▒██                      ██████          ████                  ██▒▒
▒▒▒▒▒▒██                                    ▓▓░░░░██          ████████▒▒
▒▒▒▒▒▒██                                    ▓▓░░░░██        ██░░░░██▒▒▒▒
▒▒▒▒▒▒████████        ██                      ████        ██░░░░██▒▒▒▒▒▒
▒▒▒▒▒▒▒▒██░░██      ██░░████                              ██░░░░██▒▒▒▒▒▒
▒▒▒▒▒▒▒▒██░░░░██      ██                                  ██░░░░██▒▒▒▒▒▒
▒▒▒▒▒▒▒▒▒▒██░░██                                            ██░░░░██░░▒▒
▒▒▒▒▒▒▒▒██░░░░██                ████████                      ██████▒▒▒▒
▒▒▒▒▒▒▒▒██░░██                ██░░░░░░░░██                        ██▒▒▒▒
▒▒▒▒██████████              ██░░░░░░░░░░░░██░░              ██████▒▒▒▒▒▒
▒▒██                    ████░░░░░░██████░░░░██      ████████▒▒▒▒▒▒▒▒▒▒▒▒
▒▒██                    ████░░░░██▒▒▒▒▒▒████████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
▒▒██                    ████░░██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
▒▒██                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
▒▒▒▒██                  ██████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
▒▒▒▒▒▒██████████████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

width = 960
height = 540
window = pygame.display.set_mode((width, height))
player = pygame.transform.smoothscale(pygame.image.load("content/arrow.png").convert_alpha(), (50, 50))
line = pygame.transform.smoothscale(pygame.image.load("content/line_test.png").convert_alpha(), (50, 50))
correction = 90

class EnJeu(scene.Scene):

    def load(self):
        #text = self.ui.add(uiText("In ").with_pos(hvec2(0.25, 0, 0, 0)).with_center(vec2(0,0)))
        #sub  = self.ui.add(uiText("SubText").under(text))
        #voila= self.ui.add(uiText("Et Voilà !").under(sub))
        #doWhat= self.ui.add(uiText("Do What").relative(voila, vec2(1,0)))
        #youWant= self.ui.add(uiText("you want!").relative(doWhat, vec2(1,0), hvec2(0,0.1,0,0)))
        
        self.arrow = Arrow(0.5, 0.9) # Arrow
        self.grid = Grid(10, 10)     # Bubble's grid
       
    def update(self):
        if input.isKeyPullUp(pygame.K_SPACE):
            scene.change(None)

    def draw(self):
        #nbImage = 7
        #nombreFrameParImage = 4
        #originePx = vec2(taille.x, 0)*(game.tick//nombreFrameParImage%nbImage)
        #cam.draw_image(self.bubble, hvec2(0.2, 0, 0.5, 0), vec2(256), vec2rectangleSizePx=taille, vec2rectangleOriginPx=originePx)
        #a = degree(game.tick*8)
        #cam.draw_image(self.bubble, hvec2(0.2, 0, 0.5, 0), vec2(256*(1+a.cos*0.1), 256*(1+a.sin*0.1)), vec2rectangleSizePx=taille)
    
        # Update direction of arrow
        player_pos = window.get_rect().center
        self.arrow.update_arrow(player_pos, player, window)
    

        # Render bubbles
        self.grid.render_bubbles()

"""