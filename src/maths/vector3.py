from maths import *

# 3d vector class
class vec3:
    x, y, z = (0, 0, 0)

    def __init__(self, x = 0, y = None, z = 0):
        """(x, y, 0) or (x, y, z) or (xy, xy, 0)"""
        self.x = x
        self.y = x if y == None else y
        self.z = z

    def copy(self): return vec3(self.x, self.y, self.z)

    @staticmethod
    def from_tuple(t): return vec2(t[0], t[1], t[2])
    @staticmethod
    def lerp(oldVec, newVec, newPourcent = 0.15): return vec3(lerp(oldVec.x,newVec.x, newPourcent), lerp(oldVec.y,newVec.y, newPourcent), lerp(oldVec.z,newVec.z, newPourcent))

    @property
    def have_x_length(self): return self.x != 0
    @property
    def have_y_length(self): return self.y != 0
    @property
    def have_z_length(self): return self.z != 0
    @property
    def have_length(self): return self.have_x_length or self.have_y_length  or self.have_z_length
    @property
    def ratio_xy(self): return self.x/self.y
    @property
    def ratio_yx(self): return self.y/self.x

    def get_length(self): return sqrt(self.x**2+self.y**2+self.z**2)
    def set_length(self, length):
        v = self.normalize()*length #self = v //don't seem to work
        self.x = v.x
        self.y = v.y
        self.z = v.z
    length = property(get_length, set_length) 
    def with_length(self, length):
        self.length = length
        return self

    # return a new vec2
    def normalize(self):
        r = self.copy()
        if(r.have_length == False):
            return vec2(0)
        length = r.length
        r.x /= length
        r.y /= length
        r.z /= length
        return r

    def perpendicular(self): return vec2(-self.Y, self.X, self.Z)    

    # operator overload
    def __add__(self, other): return vec3(self.x+other.x, self.y+other.y, self.z+other.z)
    def __sub__(self, other): return vec3(self.x-other.x, self.y-other.y, self.z-other.z)
    def __mul__(self, value):
        if(type(value) == vec3):
            return vec3(self.x*value.x, self.y*value.y, self.z*value.z)
        return vec3(self.x*value, self.y*value, self.z*value)
    def __truediv__(self, value):
        if(type(value) == vec3):
            return vec3(self.x/value.x, self.y/value.y, self.z/value.z)
        return vec3(self.x/value, self.y/value, self.z/value)
    
    def __eq__(self, other): return self.x == other.x and self.y == other.y and self.z == other.z
    def __ne__(self, other): return not self == other

    def __str__(self):
        return f"x:{self.x}, y:{self.y}, z:{self.z}"

    # type conversion
    def to_hvec2(self): return hvec2(0, self.x, self.y, 0)
    def to_vec2(self):  return vec2(self.x, self.y)
    def to_vec3(self):  return self

    def to_tuple_int(self): return (int(self.x), int(self.y), int(self.z))
    def to_tuple(self): return (self.x, self.y, self.z)
    
    def __str__(self):
        return f"x:{self.x}, y:{self.y}"