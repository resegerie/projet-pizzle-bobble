from maths import *

TURN_RADIAN = PI * 2
TURN_DEGREE = 360
TURN_POURCENT = 1

def degree(degree): return angle(degree * TURN_RADIAN / TURN_DEGREE)
def radian(radian): return angle(radian)

# immutable, that why there is no setter
class angle:
    __radian = 0

    def __init__(self, __radian = 0):
        self.__radian = __radian

    def get_radian(self) : return self.__radian
    radian = property(get_radian)

    def get_degree(self): return self.__radian * TURN_DEGREE / TURN_RADIAN
    degree = property(get_degree)

    # *pourcent 4 life*
    def get_pourcent(self): return self.__radian * TURN_POURCENT / TURN_RADIAN
    pourcent = property(get_pourcent)

    def copy(self, angle):
        self.__radian = angle.__radian
    # operator overload :
    def __add__(self, other): return radian(self.radian+other.radian)
    def __sub__(self, other): return radian(self.radian-other.radian)
    def __mul__(self, value): return radian(self.radian*value)
    def __truediv__(self, value): return radian(self.radian/value)

    def get_cos(self): return cos(self.radian)
    cos = property(get_cos)

    def get_cosh(self): return cosh(self.radian)
    cosh = property(get_cosh)

    def get_sin(self): return sin(self.radian)
    sin = property(get_sin)

    def get_sinh(self): return sinh(self.radian)
    sinh = property(get_sinh)

    def get_tan(self): return tan(self.radian)
    tan = property(get_tan)

    def get_tanh(self): return tanh(self.radian)
    tanh = property(get_tanh)

    def to_vec2(self):  return vec2.from_angle(self)
    
    def __str__(self): 
        return f"{self.degree}°"