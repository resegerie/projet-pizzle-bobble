from maths import *
from maths.angle import degree, radian

# 2d vector class
class vec2:
    x, y = (0, 0)

    def __init__(self, x = 0, y = None):
        """(0, 0) or (x, y) or (xy, xy)"""
        self.x = x
        self.y = x if y == None else y

    def copy(self): return vec2(self.x, self.y)

    @staticmethod
    def from_angle(angle): return vec2(angle.cos, angle.sin)
    @staticmethod
    def from_tuple(t): return vec2(t[0], t[1])
    @staticmethod
    def lerp(oldVec, newVec, newPourcent = 0.15): return vec2(lerp(oldVec.x,newVec.x, newPourcent), lerp(oldVec.y,newVec.y, newPourcent))

    @property
    def have_x_length(self): return self.x != 0
    @property
    def have_y_length(self): return self.y != 0
    @property
    def have_length(self): return self.have_x_length or self.have_y_length
    @property
    def ratio_xy(self): return self.x/self.y
    @property
    def ratio_yx(self): return self.y/self.x

    def get_length(self): return sqrt(self.x**2+self.y**2)
    def set_length(self, length):
        v = self.normalize()*length #self = v //don't seem to work
        self.x = v.x
        self.y = v.y
    length = property(get_length, set_length) 
    def with_length(self, length):
        self.length = length
        return self

    def get_angle(self): return radian(atan2(self.y, self.x))
    def set_angle(self, angle):
        v = vec2.from_angle(angle)*self.length
        self.x = v.x
        self.y = v.y
    angle = property(get_angle, set_angle) 

    def normalize(self):
        """return a new vec2"""
        r = self.copy()
        if(r.have_length == False):
            return vec2(0)
        length = r.length
        r.x /= length
        r.y /= length
        return r

    def perpendicular(self): return vec2(-self.y, self.x)    

    # operator overload
    def __add__(self, other): return vec2(self.x+other.x, self.y+other.y)
    def __sub__(self, other): return vec2(self.x-other.x, self.y-other.y)
    def __mul__(self, value):
        if(type(value) == vec2):
            return vec2(self.x*value.x, self.y*value.y)
        return vec2(self.x*value, self.y*value)
    def __truediv__(self, value):
        if(type(value) == vec2):
            return vec2(self.x/value.x, self.y/value.y)
        return vec2(self.x/value, self.y/value)
    
    def __eq__(self, other): return self.x == other.x and self.y == other.y
    def __ne__(self, other): return not self == other

    # type conversion
    def to_hvec2(self): return hvec2(0, self.x, self.y, 0) # orthonormal axis by default based on y
    def to_vec2(self):  return self
    def to_vec3(self):  return vec3(self.x, self.y, 0)

    def to_tuple_int(self): return (int(self.x), int(self.y))
    def to_tuple(self): return (self.x, self.y)
    
    def __str__(self):
        return f"x:{self.x}, y:{self.y}"