

def lerp(oldValue, newValue, newPourcent = 0.15):
    return oldValue*(1-newPourcent)+newPourcent*newValue

def limit(value, mini, maxi):
    return min(max(value, mini), maxi)