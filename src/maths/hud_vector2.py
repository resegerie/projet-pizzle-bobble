from maths import *
import game

# Stans for Hud Vec2.
# A slightly more complicated system than % that use 2 coefficient par axis
class hvec2():
    xAxis = None
    """(x based x, x based y)"""
    yAxis = None
    """(y based x, y based y)"""
    # if you resize the windows it still work

    @staticmethod
    def lerp(oldHvec2, newHvec2, newPourcent = 0.15): return hvec2(lerp(oldHvec2.x,newHvec2.x, newPourcent), lerp(oldHvec2.xBasedy, newHvec2.xBasedy, newPourcent),  lerp(oldHvec2.y,newHvec2.y, newPourcent), lerp(oldHvec2.yBasedx,newHvec2.yBasedx, newPourcent))

    # X % based on the X Axis of the windows
    def get_x(self): return self.xAxis.x
    def set_x(self, x): self.xAxis.x = x
    x = property(get_x, set_x)

    # X % based on the Y Axis of the windows
    def get_xBasedy(self): return self.xAxis.y
    def set_xBasedy(self, xBasedy): self.xAxis.y = xBasedy
    xBasedy = property(get_xBasedy, set_xBasedy)

    # Y % based on the Y Axis of the windows
    def get_y(self): return self.yAxis.y
    def set_y(self, y): self.yAxis.y = y
    y = property(get_y, set_y)

    # Y % based on the X Axis of the windows
    def get_yBasedx(self): return self.yAxis.x
    def set_yBasedx(self, yBasedx): self.yAxis.x = yBasedx
    yBasedx = property(get_yBasedx, set_yBasedx) 

    def __init__(self, x = 0, xBasedy = 0, y = 0, yBasedx = 0):
        """  In % : (0, 0, 0, 0) or (x, xBasedY, y, yBasedX)"""
        self.xAxis = vec2(x, xBasedy)
        self.yAxis = vec2(yBasedx, y)

    # builder pattern
    def with_pourcent(self, x, y = None):
        """(x,y) or (xy, xy)"""
        self.x = x
        self.y = x if y == None else y
        return self

    def with_pourcent_opposed_axis(self, xBasedy, yBasedx):
        """(x,y) or (xy, xy)"""
        self.xBasedy = xBasedy
        self.yBasedx = yBasedx
        return self

    # operator overload
    def __add__(self, other) : return hvec2(self.x+other.x, self.xBasedy+other.xBasedy, self.y+other.y, self.yBasedx+other.yBasedx)
    def __sub__(self, other) : return hvec2(self.x-other.x, self.xBasedy-other.xBasedy, self.y-other.y, self.yBasedx-other.yBasedx)
    
    def __mul__(self, value):
        if(type(value) == hvec2):
            return hvec2(self.x*value.x, self.xBasedy*value.xBasedy, self.y*value.y, self.yBasedx*value.yBasedx)
        return hvec2(self.x*value, self.xBasedy*value, self.y*value, self.yBasedx*value)

    def __eq__(self, other) : return self.x == other.x and self.y == other.y and self.xBasedy == other.xBasedy and self.yBasedx == other.yBasedx
    def __ne__(self, other) : return not self == other

    # type conversion
    def to_hvec2(self) : return self
    def to_vec3 (self) : return self.to_vec2().to_vec3()
    def to_vec2 (self) : return vec2((self.x+self.xBasedy*game.height/game.width)*game.width, (self.y+self.yBasedx*game.width/game.height)*game.height)

    def __str__ (self) : return f"x:{self.x}, xBasedy:{self.xBasedy}, y:{self.y}, yBasedx:{self.yBasedx}"
    def copy    (self) : return hvec2(self.x, self.xBasedy, self.y, self.yBasedx)