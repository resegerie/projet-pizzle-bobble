import math
import pygame

from math import degrees
from content_manager.spr_animation import animation
from content_manager.util import get_content_path, get_font, get_image
from interface import *
from maths import *


WIDTH = 960
HEIGHT = 540

# Arrow 
class Arrow(pygame.sprite.Sprite):

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.arrow_sprite = get_image("arrow") # Test (not the final arrow)

        self.img = self.arrow_sprite
        self.rotate_img = self.img

        self.x = x
        self.y = y
    
    # Update the direction of arrow based on mouse direction
    def update_arrow(self, player_rect, player, window):
        player_rect = window.get_rect()
        mx, my = pygame.mouse.get_pos()
        dx, dy = mx - player_rect.centerx, my - player_rect.centery
        angle = math.degrees(math.atan2(-dy, dx)) - 90

        if(angle > -90):
             rot_image = pygame.transform.rotate(player, angle)
             rot_image_rect = rot_image.get_rect(center = player_rect.center)
             #window.blit(rot_image, rot_image_rect.topleft)
             rot_image_line = rot_image.get_rect(center = player_rect.center)
             self.arrow_sprite = rot_image
             self.draw_arrow(player_rect)

             # Test
             #cam.draw_line(vec2(480,480), vec2(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]))

    # Draw arrow
    def draw_arrow(self, player_rect, offsetX=0, offsetY=0):
        cam.draw_image(self.arrow_sprite, hvec2().with_pourcent(self.x, self.y), vec2(50), center = vec2(0.5, 0.5))
