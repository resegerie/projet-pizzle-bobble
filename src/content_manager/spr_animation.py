import json, random, pygame
from pathlib import Path

from content_manager import *
from content_manager.util import get_sprite
from maths import *

class animation():
    """a collection of image (sprites), with one active image (sprite)"""

    sprites = None
    """[] of image"""

    index = None
    nbLoop = None
    """nb time the animation have looped"""
    tickPerFrame = None
    tickRemaningFrame = None

    def copy(self):
        a = animation()
        a.sprites = self.sprites
        a.index = self.index
        a.nbLoop = self.nbLoop
        a.tickPerFrame = self.tickPerFrame
        a.tickRemaningFrame = self.tickRemaningFrame
        return a

    @property
    def length(self): return len(self.sprites)

    def __init__(self):
        self.clear()

    def clear(self):
        self.sprites = []
        self.index = 0
        self.tickPerFrame = 1
        self.tickRemaningFrame = 0
        self.nbLoop = 0

    def from_file(self, filename, frame):
        self.clear()
        self.filename = filename

        self.sprites = []
        self.index = 0 # Index of sprite load

        self.sprite_sheet = get_image(filename).convert()
        self.meta_data = self.filename + ".png".replace('.png', '.json')
        self.count = 0

        # Load the json file of the spritesheet
        with open(get_content_path(self.meta_data)) as f:
            self.data = json.load(f)
        f.close

        for ind in range(0, frame):
            self.sprites.append(self.parse_sprite(Path(filename).stem + str(self.count+ind)))# + ".png"))
        return self
        

    # Parse the json file of the spritesheet to obtain x, y, w, h of each sprites
    def parse_sprite(self, name):
        sprite = self.data['frames'][name + ".png"]['frame']
        x, y, w, h = sprite["x"], sprite["y"], sprite["w"], sprite["h"]
        image = get_sprite(self.sprite_sheet, x, y, w, h)
        return image

    def from_rectangles(self, image, nbImage, xBegin, yBegin, witdh, height, xOffset = 0, yOffset = 0, tickPerFrame = 30):
        self.clear()
        self.tickPerFrame = tickPerFrame
        self.tickRemaningFrame = tickPerFrame
        w = image.get_width()
        x = 0
        for i in range(nbImage):
            start = xBegin+x*(witdh+xOffset)
            if(start >= w):
                x = 0
                yBegin += height+yOffset
            self.sprites.append(get_sprite(image, start, yBegin, witdh, height))
            x += 1
        return self

    # Update
    def update(self):
        self.tickRemaningFrame -= 1
        if self.tickRemaningFrame <= 0:
            self.index = (self.index+1)
            if(self.index > self.length):
                self.index %= self.length
                self.nbLoop += 1
            self.tickRemaningFrame = self.tickPerFrame
    
    def get_current(self): return self.get(self.index)
    def get(self, index): return self.sprites[index % self.length]

    def random(self): return random.choice(self.sprites)

    def __getitem__(self, index): return self.get(index)
    """bracket operator : animaton[4]"""