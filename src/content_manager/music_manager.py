import pygame

from pygame import mixer;
from content_manager.util import get_content_path

class music():

    def __init__(self, volume = 0.2, title_music = "unity.wav"):
        self.volume = volume
        self.title_music = title_music
    
    # Setter for volume
    def set_volume(self, new_volume):
        self.volume = new_volume
        mixer.music.set_volume(self.volume) 

    # Getter for volume
    def get_volume(self): 
        return self.volume
        
    def print_volume(self):
        return str(self.volume)

    # Set for the music title
    def set_title_music(self, new_title_music):
        self.new_title_music = new_title_music

    # Load the music
    def load_music(self):
        path = get_content_path("music", self.title_music)
        try:
            mixer.music.load(path)
        except:
            print("load_music : can't find " + self.title_music.rjust(20) +" ( at " + path + " )")

    # Play the music
    def play_music(self):
        # Background music
        print(self.volume)
        mixer.music.set_volume(self.volume) 
        mixer.music.play(-1) 

    def play_sound(self, title, vol = 0.3):
        path = get_content_path("music", title)
        try:
            sound = mixer.Sound(path)
            sound.set_volume(vol)
            sound.play()
        except:
            print("play_sound : can't find "+title.rjust(20) +" ( at "+path+" )")

