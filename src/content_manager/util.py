from content_manager import *
import sys, os, pygame, pygame.font
from pathlib import Path

content_path = None

__content_manager_was_init = False

def path_replace_ext(path, extension):
    return os.path.splitext(path)[0]+'.'+extension.replace('.','')

def get_content_path(*noms): return os.path.join(*([content_path]+list(noms)))

def get_font(*noms, size = 96, extension="ttf"):
    pathWithExt = path_replace_ext(get_content_path(*noms), extension)
    f = pygame.font.Font(pathWithExt, size)
    return f

def get_image(*noms, extension="png"): 
    path = get_content_path(*noms)
    pathWithExt = path_replace_ext(path, extension)
    return pygame.image.load(pathWithExt).convert_alpha()

# Thank Reynalde :)
def get_sprite(image, x, y, w, h):
    """Get the sprite at x, y with its width and height"""
    sprite = pygame.Surface((w,h), pygame.SRCALPHA)
    sprite.blit(image, (0,0), (x,y,w,h))
    return sprite.convert_alpha()

# singleton method, should only be called once
def content_manager_init():
    global content_path, __content_manager_was_init, font_main
    assert __content_manager_was_init == False, "can call init() on content.py only once !"
    __content_manager_was_init = True
    content_path = os.path.join(os.getcwd(), "content")
    print("content path:", content_path)
    import content_manager.font_manager as fonts
    fonts.init()    