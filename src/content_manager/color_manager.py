from pygame.locals import Color
from colorsys import hsv_to_rgb

def rgb(r =255, g =255, b =255, a =255): return Color(int(r),int(g),int(b),int(a))
def hsv(h =360, s =100, v =100, a =255):
    (r,g,b) = hsv_to_rgb(h/360, s/100, v/100)
    return rgb(int(r*255), int(g*255), int(b*255), a)

def lerp(c1, c2, newPourcent = 0.15): return c1.lerp(c2, newPourcent)

RED   = rgb(255, 0  , 0  )
GREEN = rgb(0  , 255, 0  )
BLUE  = rgb(0  , 0  , 255)

WHITE = rgb(255, 255, 255)
GRAY =  rgb(127, 127, 127)
BLACK = rgb(0  ,   0,   0)

PINK  = rgb(255, 0   , 255)
CYAN  = rgb(0  , 255 , 255)

YELLOW  = hsv(50)
PURPLE  = hsv(287)
ORANGE  = hsv(35)

DARK_RED   = lerp(RED  , BLACK, 0.5)
DARK_GREEN = lerp(GREEN, BLACK, 0.5)
DARK_BLUE  = lerp(BLUE , BLACK, 0.5)