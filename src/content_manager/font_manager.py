import content_manager.util
from maths import *

font_main = None

# %
center = 0.5
right  = 1
left   = 0

# % based on y
enormous = 20 / 100
big      = 12 / 100
normal   = 7  / 100
small    = 5  / 100

def init():
    global font_main
    #font_main = content_manager.util.get_font("josefin_sans")
    font_main = content_manager.util.get_font("thomasfont7")