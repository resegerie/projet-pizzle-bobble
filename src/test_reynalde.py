"""
import pygame
from content_manager.spr_animation import animation
from content_manager.avatar import Avatar
from pathlib import Path
from maths import *

#Test

pygame.init()
DISPLAY_W, DISPLAY_H = 960, 540

canvas = pygame.Surface((DISPLAY_W, DISPLAY_H))
window = pygame.display.set_mode((DISPLAY_W, DISPLAY_H))
running = True


# Animation_Sprite testing
#index = 0
#i = 0
#j = 0

#file_path = "content/bubble.png"
#file_name = Path(file_path).stem
#my_spritesheet = animation(file_name + ".png", 7)

my_avatar = Avatar()


my_avatar.set_avatar(2)
my_avatar.set_avatar_size(150, 150)

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    canvas.fill((0,0,0))
    img = my_avatar.get_avatar()
    canvas.blit(img, (0,0))
    window.blit(canvas, (0,0))
    pygame.display.update()
"""