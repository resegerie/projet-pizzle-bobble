from content_manager import *

# Names of the mouses. Stored inside variable to easily rename them

#loaded in static_load()
Tomzy, Reyky, Roucky, Lucky  = None, None, None, None
"""static_avatar. Use Roucky.name to get the name"""

static_avatars = None
"""array of static_avatar"""
MAX_AVATAR     = None

class static_avatar:
    # contains only the animation, no logic for animating
    index = None
    name = None

    color = None

    spritesheet = None

    baseAnimation = None
    greatAnimation = None
    shootAnimation = None
    dontDoThatAnimation = None
    hitAnimation = None

    def __init__(self, name, color):
        global static_avatars, MAX_AVATAR

        static_avatars.append(self)
        self.index = MAX_AVATAR
        MAX_AVATAR = len(static_avatars)

        self.name = name
        self.color = color
        self.spritesheet = get_image("avatar", name.lower())
        pxSize = 48
        self.baseAnimation  = animation().from_rectangles(self.spritesheet, 2, 1, 0, pxSize, pxSize, tickPerFrame=30)
        self.greatAnimation = animation().from_rectangles(self.spritesheet, 1, 1, pxSize, pxSize, pxSize, tickPerFrame=10)
        self.hitAnimation   = animation().from_rectangles(self.spritesheet, 1, 1, 2*pxSize, pxSize, pxSize, tickPerFrame=10)
        self.shootAnimation = animation().from_rectangles(self.spritesheet, 3, 1, 3*pxSize, pxSize, pxSize, tickPerFrame=8)
        self.dontDoThatAnimation = animation().from_rectangles(self.spritesheet, 3, 1, 4*pxSize, pxSize, pxSize, tickPerFrame=8)

def load_avatars():
    global static_avatars, MAX_AVATAR, Tomzy, Reyky, Roucky

    static_avatars = []
    MAX_AVATAR     = 0
    Tomzy = static_avatar("Tomzy",  color.RED)
    Reyky = static_avatar("Reyky",  color.PURPLE)
    Roucky= static_avatar("Roucky", color.GREEN)
    Lucky = static_avatar("Lucky",  color.WHITE)
