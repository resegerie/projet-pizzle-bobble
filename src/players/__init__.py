import players.the_static_avatar as the_static_avatar
import players.the_player as the_player
from players.the_player import *
from players.the_static_avatar import *
from players.the_active_avatar import *

def players_load():
    load_avatars()
    load_players()