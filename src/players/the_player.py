from maths.vector2 import *
from interface import *
from level import the_world
import players.the_static_avatar as the_static_avatar
import players.the_active_avatar as the_active_avatar
import input

players = []
MAX_PLAYER = 3

PLAYER_DISCONNECTED  = 0
PLAYER_HUMAN         = 1
PLAYER_CPU           = 2

class player():
    active_avatar = None
    index = None
    type = None
    mapCopy = None
    

    keyRight, keyLeft, keyUp, keyDown, keyMenu = None, None, None, None, None
    """if it is a human. The launcher will handle the cpu behavior"""
    
    def __init__(self, active_avatar, keyRight, keyLeft, keyUp, keyDown, keyMenu):
        global players
        
        self.active_avatar = active_avatar
        self.index = len(players)
        players.append(self)
        self.type = PLAYER_HUMAN 
        self.keyRight, self.keyLeft, self.keyUp, self.keyDown, self.keyMenu = keyRight, keyLeft, keyUp, keyDown, keyMenu
        assert len(players) <= MAX_PLAYER, ("can't create new player, there is already "+str(MAX_PLAYER)+" players!")

    def with_type(self, type):
        self.type = type
        return self

    def draw(self):
        self.active_avatar.draw()

    def draw_control(self):
        a = self.active_avatar
        p = a.get_pos_hitbox(vec2(1.25,0.5))
        c = color.GRAY
        size = fonts.small/cam.zoom
        cam.draw_font(p, input.getKeyName(self.keyLeft)+"  "+input.getKeyName(self.keyRight), size=size, colorForeground=c)
        cam.draw_font(p+vec2(0, -0.5), input.getKeyName(self.keyUp  ), size=size, colorForeground=c)
        cam.draw_font(p+vec2(0,  0.5), input.getKeyName(self.keyDown), size=size, colorForeground=c)

    def draw_control_at(self, pos, spacing = 0.05):
        colorOff = color.GRAY
        colorOn  = color.WHITE
        size = fonts.small/cam.zoom
        self.draw_input_text(self.keyLeft , pos+hvec2(0, -spacing, 0, 0), colorOff, colorOn, size)
        self.draw_input_text(self.keyRight, pos+hvec2(0,  spacing, 0, 0), colorOff, colorOn, size)
        self.draw_input_text(self.keyUp,    pos+hvec2(0, 0, -spacing, 0), colorOff, colorOn, size)
        self.draw_input_text(self.keyDown,  pos+hvec2(0, 0,  spacing, 0), colorOff, colorOn, size)
    
    
    def draw_input_text(self, key, pos, colorOff, colorOn, size):
        cam.draw_font(pos, input.getKeyName(key), size=size, colorForeground=colorOn if input.isKeyPressed(key) else colorOff)

    def type_to_string(self):
        if(self.type == PLAYER_HUMAN):
            return "Joueur"
        if(self.type == PLAYER_CPU):
            return "Cpu"
        return "Déconnecter"


    def default(index, keyRight, keyLeft, keyUp, keyDown, keyMenu):
        return player(the_active_avatar.active_avatar(the_static_avatar.static_avatars[index%the_static_avatar.MAX_AVATAR]), keyRight, keyLeft, keyUp, keyDown, keyMenu)

def load_players():
    global players, MAX_PLAYER

    player.default(0, pygame.K_RIGHT, pygame.K_LEFT, pygame.K_UP, pygame.K_DOWN, pygame.K_SPACE)
    player.default(1, pygame.K_d, pygame.K_q, pygame.K_z, pygame.K_s, pygame.K_SPACE)
    player.default(2, pygame.K_j, pygame.K_g, pygame.K_y, pygame.K_h, pygame.K_SPACE)
    if(MAX_PLAYER > 3):
        player.default(3, pygame.K_m, pygame.K_k, pygame.K_o, pygame.K_l, pygame.K_SPACE)
    