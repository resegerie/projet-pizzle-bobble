from email.policy import default
from content_manager import *
from maths import *
from interface import *

class active_avatar(uiSprite):
    static_avatar = None

    anim = None

    next_anim = None
    """list of next animation. When a animation is played, it is remove from the list"""

    default_animation = None
    """animation played when next_anim is empty"""

    def __init__(self, static_avatar):
        super().__init__(static_avatar.baseAnimation[0])
        self.pos    = vec2(0)
        self.hitbox = vec2(4)
        self.center = vec2(0.5, 1)
        self.static_avatar = static_avatar
        self.next_anim = []
        self.default_animation = self.static_avatar.baseAnimation
        self.anim =self.default_animation.copy()

    def playAnimationNow(self, sprAnimation, nbTime = 1):
        self.anim.nbLoop += 1
        self.playAnimation(sprAnimation, nbTime)

    def playAnimation(self, sprAnimation, nbTime = 1):
        if(self.anim == self.default_animation):
            self.anim.nbLoop += 1

        if(len(self.next_anim) < 8):
            for _ in range(nbTime):
                self.next_anim.append(sprAnimation.copy())

    def draw(self):
        self.anim.update()

        if(self.anim.nbLoop > 0):
            if(len(self.next_anim) != 0):
                self.anim = self.next_anim.pop(0)
            else:
                self.anim =self.default_animation.copy()
            self.anim.nbLoop = 0
        self.img = self.anim.get_current()
        super().draw()