# 2nd year project: PiZZle BuBBle
#
# Made by:
#   Thomas Tamagnaud
#   Reynalde Segerie
#   Mathis Roubille

# constants :
UPS = 60
FPS = UPS

Performance_FPS = None #mesured FPS

# time
tickClock = None    
tick      = None    # update counter
seconds = 0

# screen
screen = None
width  = 960
height = 540
"""width and height are smoothed when the windows is resized"""

# inputs
events    = list() #pygame event
oldEvents = list()

wallpaper = None

import time
import pygame, sys, random, content_manager.util, tmp_test_debug, scene, scenes.options, input
from pygame.locals import *
from pygame import mixer;
from maths import *
from content_manager import *
from interface.all.image import uiWallpaper as uiWallpaper
from interface.all.image import uiWallPaperBorder
from __init__ import debugMode
from level.the_world import world 

# music
audio = music()

# cpu
precision_CPU = 4
do_detect_rotation = False

def load(defaultScene):
    # It look to much like C/C++/Java/C# instead of Python
    global tickClock, tick, screen, wallpaper, precision_CPU, do_detect_rotation, audio

    print("load()")
    pygame.init()
    screen = pygame.display.set_mode((width, height), pygame.RESIZABLE | pygame.DOUBLEBUF)
    content_manager.util.content_manager_init()
    import players
    players.players_load()
    import the_wallpaper
    the_wallpaper.load_background()
    
    wallpaperNbYRepetition = 8

    wallpaper = the_wallpaper.wallpaper(uiWallpaper(the_wallpaper.tiles[4]).with_hitbox(hvec2(0,1/wallpaperNbYRepetition,1/wallpaperNbYRepetition,0)))

    audio = music()
    audio.load_music()
    audio.play_music()

    if(debugMode):
        tmp_test_debug.test()

    tickClock = pygame.time.Clock()
    tick = 0
    pygame.display.set_caption("PiZZle BuBBle")
    pygame.display.set_icon(get_image("icon"))
    
    file     = open("option.pizz", "r")
    fileTxt  = file.read()
    file.close()
    
    fileList = fileTxt.split('\n')
    audio.set_volume(float(fileList[0]))
    precision_CPU = int(fileList[2])
    do_detect_rotation = fileList[3]
        
    scene.change(defaultScene)

def unload():
    print("unload()")
    pygame.quit()
    sys.exit()

def update():
    global width, height, events, oldEvents, tick, seconds
    tick+=1
    seconds = tick/UPS
    resolution = vec2.lerp(vec2(width, height), vec2.from_tuple(pygame.display.get_surface().get_size()), 0.15)
    (width, height) = resolution.to_tuple_int()
    #(width, height) = pygame.display.get_surface().get_size()

    for event in events:
        if event.type == pygame.KEYDOWN or event.type == pygame.KEYUP:
            for i in range(len(oldEvents)):
                if event.key == oldEvents[i].key and event.type != oldEvents[i].type:
                    oldEvents.pop(i)
                    break
            oldEvents.append(event)
    events = pygame.event.get()  

    for event in events:
        if event.type == pygame.QUIT:
            scene.change(None)

    if(scene.current is not None):
        scene.current._update()
    wallpaper.update()
    #wallpaper2.update()

def draw():
    screen.fill(color.BLACK)
    wallpaper.draw()
    if(scene.current is not None):
        scene.current._draw()
        if(type(scene.current).__name__ == "Options"):
            scene.current.__class__ =  scenes.options.Options
            audio.set_volume(scene.current.update_music())
    pygame.display.update()

def gameLoop(defaultScene):
    global Performance_FPS
    load(defaultScene)

    while(scene.current is not None):
        update()
        draw()
        if(type(scene.current).__name__ != "EcranTitre"):
            if(input.isKeyPullUp(pygame.K_ESCAPE)):
                world.isAnotherWorldEmpty = False
                scene.change(scenes.ecranTitre.EcranTitre())
        Performance_FPS = tickClock.get_fps()
        tickClock.tick(UPS)
    unload()